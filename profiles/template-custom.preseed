# Preseed values that requires  user's inputs to build the appropriate
# customized ISO image.
#
# IMPROTANT NOTE: this should not be included. Instead, copy and paste the file
#                 into custom.preseed and fills in your data accordingly.



# LOCALE
# set installer's locale settings
d-i debian-installer/locale string en_US



# KEYBOARD LAYOUTS
# set installer's keyboard settings
d-i console-setup/ask_detect boolean false
d-i keyboard-configuration/xkb-keymap select us



# TIMEZONE
# Refers to: https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
# for selection
base-config tzconfig/choose_country_zone_single boolean false
d-i     time/zone       select  Asia/Pacific



# ACCOUNT SETUP
# WARNING: passwords are not secured and will be reset again in postinstall
#          scripts. You have been warned!
# root
passwd   passwd/root-password    password R00TPASSWORD
passwd   passwd/root-password-again  password R00TPASSWORD

# user
passwd   passwd/make-user    boolean true
passwd   passwd/user-fullname    string User
passwd   passwd/username     string u0
passwd   passwd/user-password    password USERP@55W0RD
passwd   passwd/user-password-again  password USERP@55W0RD
d-i user-setup/encrypt-home boolean true

# cryptsetup disk passphrase
d-i partman-crypto/passphrase password DI5KP@55WORD
d-i partman-crypto/passphrase-again password DI5KP@55WORD



# PARTITIONING SETTINGS
# set partition target disk
d-i partman-auto/disk string /dev/vda

# set partition sizing guide (max, min, percent, 100G, etc)
d-i partman-auto-lvm/guided_size string max



# BOOTLOADER
# set Grub bootloader's target disk.
# Optional if 'lilo' or 'nobootloader' is chosen
d-i grub-installer/bootdev  string /dev/vda



# NETWORK
# set OS's hostname
d-i netcfg/get_hostname string testSubject

# set OS's domain name
d-i netcfg/get_domain string testSubject

# set OS's upstream mirror location and hostname
d-i mirror/country string japan
d-i mirror/http/hostname string deb.debian.org

# set APT upgrades action after installation
d-i pkgsel/upgrade select full-upgrade



# APT
# set update policy ('none', 'unattended-upgrades', 'landscape')
# none                - no update policy
# unattended-upgrades - install security updates automatically
# landscape           - manage the system with Landscape System Software
d-i pkgsel/update-policy select unattended-upgrades

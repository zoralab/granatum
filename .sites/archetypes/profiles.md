{{- $titleWords := replace .Name "_" "-" -}}
{{- $keywords := split $titleWords " " -}}
{{- $keywords = $keywords | append "granatum" "profile" -}}
{{- $title := strings.Title $titleWords -}}
{{- $codename := lower $title -}}

<!--
+++
date = "{{ .Date }}"
title = "{{- upper $title }} Profile"
description = """
Granatum '{{ $codename }}' profile explains its simple-cdd's role and technical
specifications.
"""
keywords = {{ replace (printf "%+q" $keywords) " " ", " }}
authors = ["Granatum Team"]
draft = true
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "B) Profiles"
name = "{{- $codename -}}"
weight = 1
+++
-->

# `{{- $codename -}}` Profile
`{{- $codename -}}` profile is the WHAT profile ...

It's profile name is known as `{{- $codename -}}`.

`{{- $codename -}}` is available since `vX.Y.Z`.



## Inclusion
There are a number of ways depending on how you use Granatum.

### Granatum Templated `.conf` Method
To include `{{- $codename -}}` in Granatum templated `.conf` file, add it into
`plist`.

```bash {linenos=table,hl_lines=[4],linenostart=1}
# plist is the list of profiles to be included in this configuration file. It
# ...
# the names are separated by space.
plist="... {{ $codename }} ..."
```

### Crude Simple-CDD `.conf` Method
If you're using your own `.conf` file, then simply include `{{- $codename -}}`
into both `profiles` and `auto_profiles`:

```bash {linenos=table,hl_lines=[1,2],linenostart=1}
profiles="{{- $codename }} ..."
auto_profiles="{{- $codename }} ..."
```



## `{{- $codename -}}.downloads`
Explain all the contents inside this profile file. Otherwise, remove the
section.




## `{{- $codename -}}.excludes`
Explain all the contents inside this profile file. Otherwise, remove the
section.




## `{{- $codename -}}.packages`
Explain all the contents inside this profile file. Otherwise, remove the
section.




## `{{- $codename -}}.preseed`
Explain all the contents inside this profile file. Otherwise, remove the
section.




## `{{- $codename -}}.udebs`
Explain all the contents inside this profile file. Otherwise, remove the
section.




## `{{- $codename -}}.postinst`
Explain all the contents inside this profile file. Otherwise, remove the
section.

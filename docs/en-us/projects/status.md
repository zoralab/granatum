<!--
+++
date = "2020-01-02T22:07:21+08:00"
title = "Status"
description = """
To get to know more about Granatum Project status, please feel free to enter
here.
"""
keywords = ["status", "Granatum", "project"]
authors = ["Granatum Team"]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "Y) Projects"
weight = 1
+++
-->

# Project Status
Here is the current project status for Granatum Project.


## Sources Location
| Name          | Location    |
|:--------------|:------------|
| GitLab.com    | [https://gitlab.com/ZORALab/granatum](https://gitlab.com/ZORALab/granatum) |


## Development Health
| Branch   | Test Status | Test Coverage |
|:---------|:------------|:--------------|
| `master` | ![pipeline status](https://gitlab.com/ZORALab/granatum/badges/master/pipeline.svg) | ![coverage report](https://gitlab.com/ZORALab/granatum/badges/master/coverage.svg) |
| `staging` | ![pipeline status](https://gitlab.com/ZORALab/granatum/badges/staging/pipeline.svg) | ![coverage report](https://gitlab.com/ZORALab/granatum/badges/staging/coverage.svg) |
| `next`   | ![pipeline status](https://gitlab.com/ZORALab/granatum/badges/next/pipeline.svg) | ![coverage report](https://gitlab.com/ZORALab/granatum/badges/next/coverage.svg) |


## Release Health
![release](https://img.shields.io/badge/release%20quality-alpha-red.svg?style=for-the-badge)

| Release Location   | Location    |
|:-------------------|:------------|
| Coming Soon        | Coming Soon |



## Primary Tools
![tool- Fennec](https://img.shields.io/badge/core%20tool-Fennec-1313c3.svg?style=for-the-badge)
![tool- Simple-CDD](https://img.shields.io/badge/core%20tool-Simple--CDD-1313c3.svg?style=for-the-badge)
![contributor covenant - v2.0 adopted](https://img.shields.io/badge/Contributor%20Covenant-v2.0%20adopted-ff69b4.svg?style=for-the-badge)

<!--
+++
date = "2020-01-02T22:26:09+08:00"
title = "Contributors"
description = """
Want to know Granatum Project's contributors? Here is the right place. We
documented our contributors in this page.
"""
keywords = ["contributors", "granatum"]
authors = ["Granatum Team"]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "Y) Projects"
#name = ""
weight = 1
+++
-->

# Contributors
Here are the list of project contributors and sponsors to make Granatum project
available as open-source tool.

## Sponsors

[![ZORALab](https://lh3.googleusercontent.com/jwB6L7Jl2A9DkqolBhnR915KvgMid66zieSDqh5hMK6oMJR81mw_QWzynMEJL-R_8yA1yKXawvGy3wb3pFmfeoCu-mVrBFPhY6BaORathrfVogDNUvZ5WvKtpH9s7faaOMDP9_KKUBkWny87gnX1mNDclbJn1NqY6rF237CbYHSyqjnYaFtACRdqR7Pl4ZZeqzdaybQWsF2KWbyqSNfQA-wsMFGf9An_xxrvS-oUKqB7G_cxotfFqpK2NvVdhQ80G7scO3WHNqlhTKmcPrMCk-BlTyNCmLsaqeAw9rfWV66ty_ximBVMnOOHVOwW4hb3RCGG9X130FiaX9Vj4Retu4wSW8DpvrvBGJqfMqqYqNo4w2c0oQUiDn3sH0IKyNMuTGiegXZUt-IcLK437uwf_FGO42XBorQSB4bEt0ZoQweUVmOADM03VEivIKFaM8bI0obdBtFoOCSz29mUeF1Pa5Oc7EJPfGakC5TAf5HJsnbjYRvuVDsXshHuuZ7JH1JXs93SE_8mf-3flN3N18ORwvTIKsiGJ4t38pEk9QWqXt2PToBuCJBjBdAWFUwWEU1_NOIM8mY_rpfOH_AmH_CtX0Ql6KUF-sIcdQzKKSn4cQjICOvPfF5bSHOzXWzEwHeqvjFmkRybHH5HORdk-BaGRZQQh3so3h9m=w300-h100-no)](https://www.zoralab.com)

> Want to be here? See [Sponsoring]({{< absLangLink "/projects/sponsoring/" >}}).


## Contributors
Here are the list of contributors base on their roles and contributions. The
maintainers are the one that maintains and manage the package releases.

### Maintainers
1. (Holloway) Chew, Kean Ho (`kean.ho.chew@zoralab.com`) - 2020 to present

### Developers
1. (Holloway) Chew, Kean Ho (`kean.ho.chew@zoralab.com`) - 2020 to present

### Knowledge Expert
1. [Simple-CDD](https://salsa.debian.org/debian/simple-cdd) - 2020

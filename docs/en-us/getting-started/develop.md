<!--
+++
date = "2020-01-08T13:26:38+08:00"
title = "Develop"
description = """
This page guides you how to develop your custom configurations in your module.
"""
keywords = ["develop", "granatum", "getting-started"]
authors = ["Granatum Team"]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "A) Getting Started"
#name = ""
weight = 4
+++
-->

# Develop
Now that you have a completely working module. It's time to develop your own
custom configurations.

## Understand Your Module Structure
Your module is designed specifically as an independent git repository. This way,
you can modularize your customizations without too much copy-pasting all over
places.

### `.config`
This is `simple-cdd`'s `.conf` file. You can either develop your own or use
Granatum conf template file (latter is recommended).

This file gets copied to the workspace everytime you initiated a build.


### `profiles/custom.XXXX` profiles
This are the `custom` profile's files stored under `profiles` directory.

Only `custom` profile's files get copied to the workspace everytime you
initiated a build.


### `local_packages`
This directory holds all your custom `deb` packages.

All the contents get copied to the workspace everytime you initiated a build.


## Development Workspace
If you're using `vim` as your editing tool, I would suggest having 2 terminals
available for effective development:

* 1 terminal is at the workspace for running the build.
* 1 terminal is for editing your module repository.

This way, you save large amount of efforts changing directory between your
module and workspace.

That's all about developing your custom module. Enjoy building OS Installer!

<!--
+++
date = "2020-01-08T12:15:10+08:00"
title = "Build"
description = """
This page guides you how to prepare and build ISO image using Granatum Project.
"""
keywords = ["build", "granatum", "getting-started"]
authors = ["Granatum Team"]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "A) Getting Started"
#name = ""
weight = 2
+++
-->

# Build
After [installing]({{< absLangLink "/getting-started/install/" >}})
Granatum Project, it's time to learn how to build you own ISO image. Granatum
Project uses a POSIX-Compliant SHELL script that acts as an helper application.

This guide shows you how to build your ISO image using Granatum, written
in sequences.

## [OPTIONAL] Switch to Your Branch and Tag Version
Granatum Project maintains 3 different branches, namely:

1. `master` - The default customer release branch. It is tested and stable.
2. `staging` - The next release branch under testing.
3. `next` - the bleeding edge branch that developers uses. It can be unstable.

To switch to a particular branch, use `git`:

```bash
$ git checkout next    # for next branch
# git checkout staging # for staging branch
$ git checkout master  # for master branch
```

Alternatively, you can switch to any release tags:

```bash
$ git tag -l                 # list all tags
v1.8.5
v1.8.6
v1.8.7
v1.8.8
...
$ git checkout tags/vX.Y.Z   # switch to tag vX.Y.Z
```

## Pull Updates
If you switched to a release tag, skip this step.

As a good practice, you should always perform an update from upstream. To do
that, switch to your branch and use `git`.

```bash
$ git pull
```

This will pull all the relevant updates for your current working branch.


## Setup Module
Granatum provides `.modules` directory for you to setup your Granatum module.
Depending on your experience, please read the appropriate sub-sections to
meet your need.

Firstly, Granatum relies on an **exported environment variable** to identify
your module by name. You need to make sure the name of the module directory is
exactly the same as the mentioned variable. The exported variable name is:
`GRANATUM_MODULE`.

To export the environment variable, you can reference the following example:

```bash
$ export GRANATUM_MODULE="myModuleName"
```


### Create Module
To create a brand new module, you can use the app script located at
`.scripts/granatum.sh`. Example:

```bash
$ export GRANATUM_MODULE="myModuleName"   # in case you forgotten
$ ./.scripts/granatum.sh -cm
```

This creates a directory inside `.modules`, namely `.modules/myModuleName`.
There are a few things you need to take notes of:

1. `.modules/<NAME>/.confg` - your build configurations file (simple-cdd's
`.conf` file). You can customize accordingly but DO NOT RENAME it.
2. `.modules/<NAME>/local_packages` - all your local packages.
3. `.modules/<NAME>/profiles` - all your `custom.XXXX` profiles' file. Only
`custom.XXXX` files get copied to the workspace per build.

> **TIP**: You can enter to your module and initializes a new `git` repository.
> That way, it tracks your customization efforts in an independent version
> control manner.



### Placing My Own Module
If you have your own module developed before and wanted to place it in the
workspace, simply copy/move it into `.modules` directory. Example:

```bash
$ mv /path/to/my/module .modules/.    # to move your module
$ cp /path/to/my/module .modules/.    # to copy your module
```


## Build ISO Image
Once done, you may proceed to run the build sequences using the app script. To
build, you first need to ensure the `GRANATUM_MODULE` environment variable is
exported with your module name. Then run the script with `--build` argument.
Example:

```bash
$ export GRANATUM_MODULE="myModuleName"   # in case forgotten
$ ./.scripts/granatum.sh --build
```

Granatum automatically copies the necessary files into the workspace and build
the ISO image accordingly. If everything is successful, your ISO image filepath
is specified. Congrats!

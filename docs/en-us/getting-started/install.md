<!--
+++
date = "2020-01-08T12:01:06+08:00"
title = "Install"
description = """
This page guides you to setup Granatum Project from scratch to working into
your local system.
"""
keywords = ["install", "granatum", "getting-started"]
authors = ["Granatum Team"]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "A) Getting Started"
#name = ""
weight = 1
+++
-->

# Install
Granatum project is a data workspace for `simple-cdd` application. Rather than
complicating the project with various packages, Granatum Project distribute
using `git` version control software.

This section guides you on how to setup Granatum in your local system. They
are arranged in a sequenced manner.

> **IMPORTANT NOTE**: This project is not for faint hearted or beginner. Any
> users must have decent understanding on how Debian Operating System works
> internally before using Granatum. In short: you're a Linux-based operating
> system developer, not regular user.

## Build Engine - Operating System
Granatum Team uses **`Debian - Stable`** operating system to build the
installer ISO image.

It is noted that if you're on other Debian-derived operating system such as
Ubuntu, you are likely going to build Ubuntu installer ISO image instead. Hence,
please check your operating system's manual.

## Download Dependencies
Granatum Project depends on the following software:

1. `simple-cdd` - the ISO builder. (`$ sudo apt-get install simple-cdd -y`)
2. `git` - the version control software. (`$ sudo apt-get install git -y`)

Set them up respectively.

## Clone The Repository
The first thing to do is to clone Garantum Project into your build system using
`git`.

```bash
$ git clone git@gitlab.com:ZORALab/granatum.git
```

If you're using `https` protocol:

```bash
$ git clone https://gitlab.com/ZORALab/granatum.git
```

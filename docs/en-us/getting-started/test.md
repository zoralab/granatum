<!--
+++
date = "2020-01-08T13:00:26+08:00"
title = "Test"
description = """
This page guides you how to test the ISO image built via Granatum Project.
"""
keywords = ["test", "granatum", "getting-started"]
authors = ["Granatum Team"]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "A) Getting Started"
#name = ""
weight = 3
+++
-->

# Test
After [building]({{< absLangLink "/getting-started/build/"  >}}) your ISO image
successfully, you should test it on your target machine. This guide explains to
you how to test your ISO image.




## Building the USB Stick
Depending on your build OS, they have their own procedures to create a bootable
USB stick.

### Debian
On Debian, you should follow their documentation based on your distribution.
Here is an example for Debian Stretch:
[https://www.debian.org/releases/stretch/amd64/ch04s03.html.en](https://www.debian.org/releases/stretch/amd64/ch04s03.html.en). In any cases, you should be
doing:

```bash
$ cp debian.iso /dev/sdX
$ sync
```


## Boot and Install
Once booted into the USB stick, you should select either `graphical install`
or `install`. This will perform the automated installation designed by you.
Depending on the contents and network connections, the setup can take a very
long time. Grab a cup of coffee and wait for its completion.

![debian install prompt]({{< absLink "/img/debian-installer-prompt.png" >}})

> **IMPORTANT NOTE**: Although Granatum strives to build headless installation,
> you still need to watch over it just in case there are new prompts or
> failures due to misconfigurations.
>
> **ONLY WALK AWAY WHEN THE HEADLESS INSTALLATION IMAGE IS TESTED WORKING
> PROPERLY.**

> **TIP**: During the installation while testing, we suggest you do
> `CTRL+ALT+F4` for its debug console. This way, you can know what's going on
> from the backend.
>
> To switch back to frontend, it is `CTRL+ALT+F1` if you selected install or
> `CTRL+ALT+F5` if you selected graphical install.


## Issues and Report
If you believe you found an issue, feel free to file them for discussion here:
[https://gitlab.com/ZORALab/granatum/issues](https://gitlab.com/ZORALab/granatum/issues).

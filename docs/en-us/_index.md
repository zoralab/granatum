<!--
+++
date = "2020-01-02T21:32:27+08:00"
title = "Home"
description = """
Granatum project is a consolidated recipes for building custom Debian ISO. Visit
us to find out more.
"""
keywords = ["Granatum", "Debian", "ISO", "builder"]
authors = ["Granatum Team", "Holloway Chew Kean Ho"]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = ""
# name = ""
weight = 1
+++
-->

# ![Granatum Project]({{< absLink "img/logo/banner-2500x1250-color.png" >}})
Granatum Project is a consolidated recipes for building custom Debian ISO using
[simple-CDD](https://salsa.debian.org/debian/simple-cdd). Its aim is to unify
multiple Debian OS image builds under a single workspace for providing a
smooth and headless Debian installations with various configurable
customizations.

If you're interested with the source codes, feel free to visit:

> https://gitlab.com/ZORALab/granatum

Otherwise, here's a good start:
[Getting Started - Install]({{< absLangLink "/getting-started/install/" >}})

## Notable Features
Granatum Project has the following notable features:

1. ✅ - Debian Stable Suite (`v0.1.0`)
2. ✅ - Modularize User-Specific Profiles (`v0.1.0`)
3. ✅ - Standard Full Disk Encryption (`v0.1.0`)
4. ✅ - Grub Bootloader Profile (`v0.1.0`)
5. ✅ - LILO Bootloader Profile (`v0.1.0`)
6. ✅ - No Bootloader Profile (`v0.1.0`)
7. ✅ - `ufw` Profile (`v0.1.0`)
8. ✅ - `tree` Profile (`v0.1.0`)
9. ✅ - `haveged` Profile (`v0.1.0`)
10. ✅- `lxqt`+`sddm` Desktop Profile (`v0.1.0`)
11. 📅 - Detached USB Key Full Disk Encryption (include `/boot`)
12. ✅ - Plymouth Profile (`v0.2.0`)
13. ❌ - SAMBA Profile (`v0.3.0`)
14. 📅 - Go Programming Language Profile
15. 📅 - Automatic Post Install Password Reset for All
16. ✅ - SSHFS Profile (`v0.2.0`)
17. ✅ - GnuPG Profile (`v0.2.0`)
18. ✅ - VIM Profile (`v0.2.0`)
19. ✅ - `firmware-linux-nonfree` (`fw-core`) Profile (`v0.2.0`)
20. ✅ - `xserver-xorg-video-ati` (`fw-ati`) Profile (`v0.2.0`)
21. ✅ - OpenSSH Client Profile (`v0.2.0`)
22. ✅ - OpenSSH Server Profile (`v0.2.0`)
23. ✅ - Wget Profile (`v0.3.0`)
24. ✅ - Curl Profile (`v0.3.0`)
25. ✅ - `sddm` auto-login Profile (`v0.3.0`)
26. ✅ - `x` toucpad tapping Profile (`v0.3.0`)
27. ✅ - `resolvconf` Profile (`v0.3.0`)
28. ✅ - `qbittorrent` Profile (`v0.3.0`)


| Legends | Descriptions                                |
|:--------|:--------------------------------------------|
| ✅      | Completed.                                  |
| 📅      | Confirmed Proposal. Currently Developing... |
| ❌      | Rejected Idea.                              |
| 💡      | Considered Ideas. Still Discussing...       |

For more information, visit our Issues board at:
https://gitlab.com/ZORALab/granatum/issues

## Sponsorship
This project is sponsored by:

[![ZORALab](https://lh3.googleusercontent.com/jwB6L7Jl2A9DkqolBhnR915KvgMid66zieSDqh5hMK6oMJR81mw_QWzynMEJL-R_8yA1yKXawvGy3wb3pFmfeoCu-mVrBFPhY6BaORathrfVogDNUvZ5WvKtpH9s7faaOMDP9_KKUBkWny87gnX1mNDclbJn1NqY6rF237CbYHSyqjnYaFtACRdqR7Pl4ZZeqzdaybQWsF2KWbyqSNfQA-wsMFGf9An_xxrvS-oUKqB7G_cxotfFqpK2NvVdhQ80G7scO3WHNqlhTKmcPrMCk-BlTyNCmLsaqeAw9rfWV66ty_ximBVMnOOHVOwW4hb3RCGG9X130FiaX9Vj4Retu4wSW8DpvrvBGJqfMqqYqNo4w2c0oQUiDn3sH0IKyNMuTGiegXZUt-IcLK437uwf_FGO42XBorQSB4bEt0ZoQweUVmOADM03VEivIKFaM8bI0obdBtFoOCSz29mUeF1Pa5Oc7EJPfGakC5TAf5HJsnbjYRvuVDsXshHuuZ7JH1JXs93SE_8mf-3flN3N18ORwvTIKsiGJ4t38pEk9QWqXt2PToBuCJBjBdAWFUwWEU1_NOIM8mY_rpfOH_AmH_CtX0Ql6KUF-sIcdQzKKSn4cQjICOvPfF5bSHOzXWzEwHeqvjFmkRybHH5HORdk-BaGRZQQh3so3h9m=w300-h100-no)](https://www.zoralab.com)

> Want to be here? See:
> [Sponsoring Section]({{< absLangLink "/projects/sponsoring/" >}}).

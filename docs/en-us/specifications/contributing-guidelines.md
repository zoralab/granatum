<!--
+++
date = "2020-01-04T13:38:47+08:00"
title = "Contributing Guidelines"
description = """
To know how to contribute back to Granatum Project, this is the right page. It
has the summary of the guides.
"""
keywords = ["contributing", "guidelines", "granatum"]
authors = ["Granatum Team"]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "Z) Specifications"
#name = ""
weight = 1
+++
-->

# Contributing Guidelines
Thank you for your interests in contributing back to Granatum Project. We do
facilitate some contributing guidelines for keeping things in order.

## Comply to Our Contributing Agreement
Please read through our
[Contributing Agreement]({{< absLangLink "specifications/contributing-agreements" >}}) before submitting a merge request. Once your codes are merged into the
upstream, we strictly assumed you agreed to the agreements willingly and there
will be no further disputes.

## Comply to Our Codes of Conduct
We have a
[Codes of Conduct]({{< absLangLink "specifications/codes-of-conduct/" >}}) to
ensure all stakeholders are well mannered.

## Comply to Specifications
This documentations has a specfication section. This section holds all the
design and technical information regarding Granatum Project. Please ensure
your contributions are complying to the specs and do not break any specified
technicalities.

## Upstream Process
We have a **VERY STRICT**
[Upstream Process]({{< absLangLink "specifications/upstream-process/" >}}).
Please read them carefully to avoid unncessary rejections. Once you're done,
feel free to contact us and we are looking forward to collaborate with you.

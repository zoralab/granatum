<!--
+++
date = "2020-01-04T15:44:26+08:00"
title = "Contributing Agreements"
description = """
This page states the Granatum Project contributing agreements in full contents.
Please read them carefully before contribute.
"""
keywords = ["contributing", "agreements", "granatum"]
authors = ["Granatum Team"]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "Z) Specifications"
#name = ""
weight = 1
+++
-->

# Contributing Agreement Licenses
As you contribute back your codes, you're automatically agreed to the following
licenses. Depending on how you contribute back (as independent individual or
corporate), you may read through the agreement carefully before submitting your
contributions.


## Individual Contributor License Agreement
This section is the individual license agreement.

```
INDIVIDUAL CONTRIBUTOR LICENSE AGREEMENT

You accept and agree to the following terms and conditions for Your present and
future Contributions submitted to Granatum Project. Except for the license
granted herein to ZORALab Enterprise and recipients of software distributed by
ZORALab Enterprise, You reserve all right, title, and interest in and to Your
Contributions.

1.  Definitions.

  "You" (or "Your") shall mean the copyright owner or legal entity authorized
  by the copyright owner that is making this Agreement with ZORALab Enterprise.
  For legal entities, the entity making a Contribution and all other entities
  that control, are controlled by, or are under common control with that
  entity are considered to be a single Contributor. For the purposes of this
  definition "control" means
    (i) the power, direct or indirect, to cause the direction or management of
        such entity, whether by contract or otherwise, or
   (ii) ownership of fifty percent (50%) or more of the outstanding shares, or
  (iii) beneficial ownership of such entity.

  "Contribution" shall mean any original work of authorship, including any
  modifications or additions to an existing work, that is intentionally
  submitted by You to ZORALab for inclusion in, or documentation of, any of the
  products owned or managed by ZORALab Enterprise (the "Work"). For the
  purposes of this definition, "submitted" means any form of electronic, verbal,
   or written communication sent to ZORALab Enterprise or its representatives,
  including but not limited to communication on electronic mailing lists, source
  code control systems, and issue tracking systems that are managed by, or on
  behalf of, ZORALab Enterprise for the purpose of discussing and improving the
  Work, but excluding communication that is conspicuously marked or otherwise
  designated in writing by You as "Not a Contribution."

2.  Grant of Copyright License.

  Subject to the terms and conditions of this Agreement, You hereby grant to
  ZORALab Enterprise and to recipients of software distributed by
  ZORALab Enterprise a perpetual, worldwide, non-exclusive, no-charge,
  royalty-free, irrevocable copyright license to reproduce, prepare derivative
  works of, publicly display, publicly perform, sublicense, and distribute Your
  Contributions and such derivative works.

3.  Grant of Patent License.

  Subject to the terms and conditions of this Agreement, You hereby grant to
  ZORALab Enterprise and to recipients of software distributed by
  ZORALab Enterprise a perpetual, worldwide, non-exclusive, no-charge,
  royalty-free, irrevocable (except as stated in this section) patent license
  to make, have made, use, offer to sell, sell, import, and otherwise transfer
  the Work, where such license applies only to those patent claims licensable by
  You that are necessarily infringed by Your Contribution(s) alone or by
  combination of Your Contribution(s) with the Work to which such
  Contribution(s) was submitted. If any entity institutes patent litigation
  against You or any other entity (including a cross-claim or counterclaim in
  a lawsuit) alleging that your Contribution, or the Work to which you have
  contributed, constitutes direct or contributory patent infringement, then any
  patent licenses granted to that entity under this Agreement for that
  Contribution or Work shall terminate as of the date such litigation is filed.

4.  You represent that you are legally entitled to grant the above license.

  If your employer(s) has rights to intellectual property that you create that
  includes your Contributions, you represent that you have received permission
  to make Contributions on behalf of that employer, that your employer has
  waived such rights for your Contributions to ZORALab, or that your employer
  has executed a separate Corporate CLA with ZORALab.

5.  You represent that each of Your Contributions is Your original creation
(see section 7 for submissions on behalf of others).

  You represent that Your Contribution submissions include complete details of
  any third-party license or other restriction (including, but not limited to,
  related patents and trademarks) of which you are personally aware and which
  are associated with any part of Your Contributions.

6.  You are not expected to provide support for Your Contributions, except to
the extent You desire to provide support.

You may provide support for free, for a fee, or not at all. Unless required by
applicable law or agreed to in writing, You provide Your Contributions on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
implied, including, without limitation, any warranties or conditions of TITLE,
NON- INFRINGEMENT, MERCHANTABILITY, or FITNESS FOR A PARTICULAR PURPOSE.

7.  Should You wish to submit work that is not Your original creation, You may
submit it to ZORALab Enterprise separately from any Contribution, identifying
the complete details of its source and of any license or other restriction
(including, but not limited to, related patents, trademarks, and license
agreements) of which you are personally aware, and conspicuously marking the
work as "Submitted on behalf of a third-party: [insert_name_here]".

8.  You agree to notify ZORALab Enterprise of any facts or circumstances of
which you become aware that would make these representations inaccurate in any
respect.

This text is licensed under the Creative Commons Attribution 3.0 License
(https://creativecommons.org/licenses/by/3.0/) and the original source is the
Google Open Source Programs Office.
```


## Corporate Contributor License Agreement
This section is the corporate contributor license agreement.

```
CORPORATE CONTRIBUTOR LICENSE AGREEMENT

You accept and agree to the following terms and conditions for Your present and
future Contributions submitted to ZORALab Enterprise. Except for the license
granted herein to ZORALab Enterprise and recipients of software distributed by
ZORALab Enterprise, You reserve all right, title, and interest in and to Your
Contributions.

1.  Definitions.

  "You" (or "Your") shall mean the copyright owner or legal entity authorized
  by the copyright owner that is making this Agreement with ZORALab Enterprise.
  For legal entities, the entity making a Contribution and all other entities
  that control, are controlled by, or are under common control with that entity
  are considered to be a single Contributor. For the purposes of this
  definition, "control" means
    (i) the power, direct or indirect, to cause the direction or management of
        such entity, whether by contract or otherwise, or
   (ii) ownership of fifty percent (50%) or more of the outstanding shares, or
  (iii) beneficial ownership of such entity.

  "Contribution" shall mean the code, documentation or other original works of
  authorship, including any modifications or additions to an existing work,
  that is submitted by You to ZORALab Enterprise for inclusion in, or
  documentation of, any of the products owned or managed by ZORALab Enterprise
  (the "Work"). For the purposes of this definition, "submitted" means any form
  of electronic, verbal, or written communication sent to ZORALab Enterprise or
  its representatives, including but not limited to communication on electronic
  mailing lists, source code control systems, and issue tracking systems that
  are managed by, or on behalf of, ZORALab Enterprise for the purpose of
  discussing and improving the Work, but excluding communication that is
  conspicuously marked or otherwise designated in writing by You as
  "Not a Contribution."

2.  Grant of Copyright License.

Subject to the terms and conditions of this Agreement, You hereby grant to
ZORALab Enterprise and to recipients of software distributed by
ZORALab Enterprise a perpetual, worldwide, non-exclusive, no-charge,
royalty-free, irrevocable copyright license to reproduce, prepare derivative
works of, publicly display, publicly perform, sublicense, and distribute Your
Contributions and such derivative works.

3.  Grant of Patent License.

Subject to the terms and conditions of this Agreement, You hereby grant to
ZORALab Enterprise and to recipients of software distributed by
ZORALab Enterprise a perpetual, worldwide, non-exclusive, no-charge,
royalty-free, irrevocable (except as stated in this section) patent license to
make, have made, use, offer to sell, sell, import, and otherwise transfer the
Work, where such license applies only to those patent claims licensable by You
that are necessarily infringed by Your Contribution(s) alone or by combination
of Your Contribution(s) with the Work to which such Contribution(s) was
submitted. If any entity institutes patent litigation against You or any other
entity (including a cross-claim or counterclaim in a lawsuit) alleging that your
Contribution, or the Work to which you have contributed, constitutes direct or
contributory patent infringement, then any patent licenses granted to that
entity under this Agreement for that Contribution or Work shall terminate as of
the date such litigation is filed.

4.  You represent that You are legally entitled to grant the above license.

You represent further that each of Your employees is authorized to submit
Contributions on Your behalf, but excluding employees that are designated in
writing by You as "Not authorized to submit Contributions on behalf of
[name of Your corporation here]." Such designations of exclusion for
unauthorized employees are to be submitted via email to
[legal@zoralab.com](mailto:legal@zoralab.com).

5.  You represent that each of Your Contributions is Your original creation
(see section 7 for submissions on behalf of others).

6.  You are not expected to provide support for Your Contributions, except to
the extent You desire to provide support. You may provide support for free, for
a fee, or not at all. Unless required by applicable law or agreed to in writing,
You provide Your Contributions on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied, including, without
limitation, any warranties or conditions of TITLE, NON-INFRINGEMENT,
MERCHANTABILITY, or FITNESS FOR A PARTICULAR PURPOSE.

7.  Should You wish to submit work that is not Your original creation, You may
submit it to ZORALab Enterprise separately from any Contribution, identifying
the complete details of its source and of any license or other restriction
(including, but not limited to, related patents, trademarks, and license
agreements) of which you are personally aware, and conspicuously marking the
work as "Submitted on behalf of a third-party: [named here]".

8.  It is Your responsibility to notify GitLab.com when any change is required
to the list of designated employees excluded from submitting Contributions on
Your behalf per Section 4. Such notification should be sent via email to
[legal@zoralab.com](mailto:legal@zoralab.com).

This text is licensed under the Creative Commons Attribution 3.0 License
(https://creativecommons.org/licenses/by/3.0/) and the original source is the
Google Open Source Programs Office.

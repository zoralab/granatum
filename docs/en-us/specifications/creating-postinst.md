<!--
+++
date = "2020-01-15T22:56:20+08:00"
title = "Creating .POSTINST"
description = """
This section is about specifications related to creating profiles/XXX.postinst
files.
"""
keywords = ["creating", "postinst", "granatum", "specifications", "specs"]
authors = ["Granatum Team"]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "Z) Specifications"
#name = ""
weight = 1
+++
-->

# Creating `.postinst`
`.postinst` is a very powerful shell script used for performing any shell
commands after the operating system is completely installed.

Each `.postinst` script should:

1. be POSIX compliant SHELL script
2. operate independently without sourcing other scripts




## Reference and Development File
`template-custom.postinst` is the reference file for developing the `.postinst`
script.

It has all the required automation good enough to build a new
`profile.postinst` by just simply duplicate it.

> **NOTE**: as a developer, you should always focus on developing
> `template-custom.postinst` in order to enhance its performance for users to
> build their `custom.postinst`.




## Internal Variables
There are internal variables set to ensure the script is working properly.
These variables are declared under `SCRIPT VARIABLES` section.

```sh
...
#####################
# SCRIPT VARIABLES #
#####################
_some_internal_function() {
    ...
}

...
```


### `postinst_name`
This is an automated filename extracted from its caller. It is required for
logging the `postinst` execution.

You do not need to change anything as it will extract the filename on its own.


### `log_path`
This is a unified log path for saving all Granatum Project's log data. It is
set to `/var/log/granatum_postinst.log` as a constant.




## Internal Functions
There are internal functions meant to unify some common yet critical operations.
Internal functions are written under the `SCRIPT LIBRARIES` section of the
script.

```sh
...
#####################
# SCRIPT LIBRARIES  #
#####################
_some_internal_function() {
    ...
}

...
```


### `_print_status`
This is meant for standardizing printing out status messages.
A `_print_status` takes 2 inputs where the first one is an indicator consisting:

1. `warning`
2. `debug`
3. `error`
4. `info` (default argument is not recognizable)

The following would be the contents of the messages. The output is both printed
on `stderr` and into the specified `log_path`.


### `_log`
This is to save the both `stdout` and `stderr` into the specified `log_path`
when a given command is provided to this function.


## Developing Your Codes
Your codes should be developed after `SCRIPT EXECUTIONS` section. There are
things to consider in order to develop a proper working `.postinst` script.

```sh
...
#####################
# SCRIPT EXECUTIONS #
#####################
echo "my code here"
```


### Use Internal Functions As First Choice
Whenever an internal function is a suitable candidate, use it. Please avoid
rolling your own similar function and get everyone confused.


### Update to Installer Status
Currently, there is no way to directly interact with the installer's status.

Since `simple-cdd` `.postinst` runs after change root, it is impossible to
access `log-output` binary from the installer environment.


### Access ISO Files
To access iso files, you can browse it under: `/media/cdrom0` or equivalent.

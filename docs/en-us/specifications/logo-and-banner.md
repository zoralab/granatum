<!--
+++
date = "2020-01-06T10:37:30+08:00"
title = "Logo and Banner"
description = """
This page holds Granatum Project's logo and banner. It also describes how to
use the logo.
"""
keywords = ["logo", "banner", "granatum"]
authors = ["Granatum Team"]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "Z) Specifications"
#name = ""
weight = 1
+++
-->

# Logo and Banner
Granatum Project's logo and banner is hand-crafted using Google Drawing.


## Drawing Board
The drawing board is available at Google Drawing location:
```
https://drive.google.com/drive/folders/1uj05V4vXkpmMyvtK1WLgijM2rSIvRfEI
```

It is hand-crafted by "Holloway" Chew, Kean Ho himself.


## Usage Policy
While the Project is under open-source license, you should deploy Granatum
logo and banner in the following manners:

1. **DO NOT** alter the logo and banner in any means with
**only exception to porportional resizing**.
2. **DO NOT** sell unaltered copies of the logo and banner as a stock photo,
poster, print or on a physical product.
3. **DO NOT** imply endorsement of your product with the images.
4. **DO NOT** redistribute without consent.
5. **DO NOT** use the images for bad faith or appear in a bad light or in a
way that is offensive.


## Logo
This is the square logo for Granatum Project.

### PNG - square 2048px - color - with background
The image is available at this location (non-CDN supported):

```
{{< absLink "img/logo/logo-s2048-color.png" >}}
```

as:

![logo]({{< absLink "img/logo/logo-s2048-color.png" >}})


## Banner
This is the banner for Granatum Project used on the landing page.

### PNG - 2500x1250 - color - transparent background

The image is available at this location (non-CDN supported):

```
{{< absLink "img/logo/banner-2500x1250-color.png" >}}
```

as:

![logo]({{< absLink "img/logo/banner-2500x1250-color.png" >}})

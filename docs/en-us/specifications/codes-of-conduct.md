<!--
+++
date = "2020-01-04T15:37:59+08:00"
title = "Codes of Conduct"
description = """
This specification holds the contributors' codes of conduct. Please read before
forking the repository
"""
keywords = ["codes", "codes of coduct", "conduct", "granatum"]
authors = ["Granatum Team"]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "Z) Specifications"
#name = ""
weight = 1
+++
-->

# Code of Conduct
Starting from 2020, Granatum project is complying to Contributor Convenant
Code of Conduct 2.0. This is to ensure we are aligned with the software
industry's direction towards a friendly and developing open-source environment.

Although there is a code of conduct in place, we still want to foster a friendly
place for new ideas and developments, to the lowest age of 12. That's our
common goal.

![contributor covenant - v2.0 adopted](https://img.shields.io/badge/Contributor%20Covenant-v2.0%20adopted-ff69b4.svg?style=for-the-badge)

## Enforcement
Depending on severity, Granatum team recommends these mechanisms to protect all
project's stakeholders.

### Physical and Monetary Abusements
Immediately:

1. Contact local authority such as calling `999` or `911`.
2. Go to the nearest polic station immediately without cleaning up evidence.

> **IMPORTANT NOTE**: Both ZORALab and Granatum team work better together with
> the local authorities for purging these abusements.

### Think Concequences Before Act (Including Talk)

Depending on where you're from and how you're culturally nurished, we believe
there is one common algorithm to achieve the goal:
**think about concequence before acting.**

***The easiest way to do it is be a data-driven person***. By gathering
measurable data to support your act, it helps you earn the trust and make
informed decision.

Some examples:

1. When you need the word "think", "maybe", "guess" in your expression, work the
   research out and gather data. Otherwise, keep it silent, and keep it to
   yourself.
2. Do not talk about performance without a table of measured data. That's
   considered offensive to developers and maintainers even you try to sweet talk
   them.
3. Are there anyway to frame your message so that it leaves positive outcome,
   be it a bad news? Work it out.

> *"If you start focusing on the next job, then you're probably not going to
> do the one you have very well."* — **William H. McRaven**,
> United States Navy Four-Star Admiral

### Trust is Earned, Not Given
No one likes empty promises and lies. Hence, Bissetii team perfers to work
with earning trust instead of given. That means it takes time to prove your
words.

Some examples:
1. Complete the small tasks instead asking for big one or talking that you can
   handle big one.
   (**Rationale: data-driven by your action and be result-oriented.**)
2. Do bother starting a new one if you complete what you have now.
   (**Rationale: stay focused to produce quality**)

> *"If you can't do the little things right, you will never do the big things
> right."*  — **William H. McRaven**, United States Navy Four-Star Admiral

### Keep Learning, Not Bloasting
Technologies evolve repidly time after time. Always keep the spirit of learning,
not showing-off or showing authority. It's about wisdom, experience, and
knowledge in a domain, not domain about you. Blog it if you want your own
domain.

Some examples:

1. Sometimes, a 6-years old can creates simple algorithm much better than a
   stressed out 30-years old specialized engineer.
2. Ideas are abundance, learn all of them. Only decide when it comes to
   application. In other word, stay open for new ideas.
3. A 8-years old boy solved a near-divoce problem for his parent at that time,
   works much better than lawyers.

> *"Mastery begins with humility"* — **Robin Sharma**, Canadian writer


### Enforcement Contacts
Depending on severity and issue contents, you can raise the discussion in
our repository's issues section:
[https://gitlab.com/ZORALab/granatum/issues](https://gitlab.com/ZORALab/granatum/issues).

If you believe you need a legal channel to communicate with, we have an open
email channel. We will always read them but we are not obliged to reply all
of them. The email is available at: legal@zoralab.com

> **NOTE**: In any case, use the issues section.

## Contributor Covenant Code of Conduct Full Body

```
Contributor Covenant Code of Conduct



Our Pledge

We as members, contributors, and leaders pledge to make participation in our
community a harassment-free experience for everyone, regardless of age, body
size, visible or invisible disability, ethnicity, sex characteristics, gender
identity and expression, level of experience, education, socio-economic status,
nationality, personal appearance, race, religion, or sexual identity and
orientation.

We pledge to act and interact in ways that contribute to an open, welcoming,
diverse, inclusive, and healthy community.



Our Standards

Examples of behavior that contributes to a positive environment for our
community include:

* Demonstrating empathy and kindness toward other people
* Being respectful of differing opinions, viewpoints, and experiences
* Giving and gracefully accepting constructive feedback
* Accepting responsibility and apologizing to those affected by our mistakes,
  and learning from the experience
* Focusing on what is best not just for us as individuals, but for the overall
  community



Examples of unacceptable behavior include:


* The use of sexualized language or imagery, and sexual attention or advances
  of any kind
* Trolling, insulting or derogatory comments, and personal or political attacks
* Public or private harassment
* Publishing others’ private information, such as a physical or email address,
  without their explicit permission
* Other conduct which could reasonably be considered inappropriate in a
  professional setting



Enforcement Responsibilities

Community leaders are responsible for clarifying and enforcing our standards
of acceptable behavior and will take appropriate and fair corrective action
in response to any behavior that they deem inappropriate, threatening,
offensive, or harmful.

Community leaders have the right and responsibility to remove, edit, or reject
comments, commits, code, wiki edits, issues, and other contributions that are
not aligned to this Code of Conduct, and will communicate reasons for
moderation decisions when appropriate.



Scope

This Code of Conduct applies within all community spaces, and also applies when
an individual is officially representing the community in public spaces.
Examples of representing our community include using an official e-mail
address, posting via an official social media account, or acting as an
appointed representative at an online or offline event.



Enforcement

Instances of abusive, harassing, or otherwise unacceptable behavior may be
reported to the community leaders responsible for enforcement by emailing to
legal@zoralab.com. All complaints will be reviewed and investigated promptly
and fairly.

All community leaders are obligated to respect the privacy and security of the
reporter of any incident.



Enforcement Guidelines

Community leaders will follow these Community Impact Guidelines in determining
the consequences for any action they deem in violation of this Code of Conduct:

1. Correction

Community Impact: Use of inappropriate language or other behavior deemed
unprofessional or unwelcome in the community.

Consequence: A private, written warning from community leaders, providing
clarity around the nature of the violation and an explanation of why the
behavior was inappropriate. A public apology may be requested.

2. Warning

Community Impact: A violation through a single incident or series of actions.

Consequence: A warning with consequences for continued behavior. No interaction
with the people involved, including unsolicited interaction with those
enforcing the Code of Conduct, for a specified period of time. This includes
avoiding interactions in community spaces as well as external channels like
social media. Violating these terms may lead to a temporary or permanent ban.

3. Temporary Ban

Community Impact: A serious violation of community standards, including
sustained inappropriate behavior.

Consequence: A temporary ban from any sort of interaction or public
communication with the community for a specified period of time. No public or
private interaction with the people involved, including unsolicited interaction
with those enforcing the Code of Conduct, is allowed during this period.
Violating these terms may lead to a permanent ban.

4. Permanent Ban

Community Impact: Demonstrating a pattern of violation of community standards,
including sustained inappropriate behavior,  harassment of an individual, or
aggression toward or disparagement of classes of individuals.

Consequence: A permanent ban from any sort of public interaction within the
project community.



Attribution

This Code of Conduct is adapted from the Contributor Covenant, version 2.0,
available at:
https://www.contributor-covenant.org/version/2/0/code_of_conduct.html

Community Impact Guidelines were inspired by Mozilla’s code of conduct
enforcement ladder.

For answers to common questions about this code of conduct, see the FAQ at:
https://www.contributor-covenant.org/faq

Translations are available at:
https://www.contributor-covenant.org/translations
```

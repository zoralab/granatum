<!--
+++
date = "2020-01-08T16:30:13+08:00"
title = "Reading Materials"
description = """
This specification lists out all important and related reading materials useful
for developing Granatum Project.
"""
keywords = ["reading", "materials", "granatum"]
authors = ["Granatum Team"]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "Z) Specifications"
#name = ""
weight = 1
+++
-->

# Reading Materials
Need more information to satisfy your curiosity? This page is the right place!
It documents all the necessary and good-to-read materials related to Granatum
Project.

## Simple-CDD Documentation
`Simple-CDD` does not have a dedicated site for its documentations. The easiest
to learn about it is to read its source codes. The site is available here:

* https://salsa.debian.org/debian/simple-cdd/tree/master


## Debian Installer Internals
This is the manual for debian installer internal specifications.

* https://d-i.debian.org/doc/internals/index.html
* https://github.com/xobs/debian-installer/blob/master/doc/devel/partman-auto-recipe.txt
* https://gist.github.com/fernandolopez/1437043

## Preseed Recipes
Due to the non-centralized `simple-cdd` documentations locations, the best
way to refer preseed instructions is to read various sources. In any cases,
try scraping the internet from various sources. Here are some good references:

* https://www.debian.org/releases/stable/s390x/apbs04.en.html
* https://www.debian.org/releases/stretch/example-preseed.txt
* https://www.debian.org/releases/jessie/example-preseed.txt
* https://d-i.debian.org/manual/example-preseed.txt
* https://wiki.ubuntu.com/Enterprise/WorkstationAutoinstallPreseed
* https://www.linuxjournal.com/content/preseeding-full-disk-encryption
* https://github.com/nuada/ubuntu-preseed/blob/master/partitions-uefi-boot-root-swap.template
* https://github.com/turnkeylinux/di-live
* https://sven.stormbind.net/blog/posts/deb_uefi_pxe_install_hpe_dl120/
* https://www.chucknemeth.com/debian-9-preseed-uefi-encrypted-lvm/
* https://salsa.debian.org/installer-team

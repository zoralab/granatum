<!--
+++
date = "2020-01-04T11:13:01+08:00"
title = "User Experiences"
description = """
This page holds the specification for Granatum Project's design aspects related
to user experiences.
"""
keywords = ["user", "experiences", "granatum"]
authors = ["Granatum Team"]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "Z) Specifications"
#name = ""
weight = 1
+++
-->

# User Experiences
This page serves as the user experiences specification for Granatum Project.
It covers both users and contributors experiences.



## Target Audience
Granatum Project main target audience is for developer, dev-ops, and tech
hobbyist who are **passionate** about maintaining long-term OS installation
with specific customization. When we say "users" in any of our documents,
we refers to them.

Since Debian operating system is a very complex and wide domain, it demands
any users and contributors to have not only breadth but also, depth knowledge
related to the Linux operating system itself.

To avoid having too many wanna-bes creating a load of homework questions (as in
never reads documentations or digging the internet for information before
asking that is annoying to the Debian community and across the Internet), the
documentations here are designed in a way that it demands a minimum level of
Linux OS (specifically Debian) understanding and skill mastery.



## `.config` and `profiles/custom.XXXX`
To keep the workspace sane and clean enough for both users and contributors,
Granatum Project uses:

1. `$PWD/.config` that is the `simple-cdd .conf` file for builds
2. Reserving `custom` label for any user's customizations in `profiles/`
directory. (e.g. `$PWD/profiles/custom.packages`,
`$PWD/profiles/custom.preseed`)

Hence, users must setup his/her own `.config` and `custom.XXXX` profiles into
the workspace before building a Granatum assisted ISO image.

> **NOTE**: Both `.config` and `profiles/custom.XXXX` are already added into the workspace's `.gitignore` file.



## Semi-Automation Scripts/App
Granatum Project is planning to use an automation script/app to perform some
automation work. However, at this point in time, further analysis is still
needed to ensure the Project's sustainability is managable.

Currently, there is a BASH script available at `.scripts` directory for use:
```bash
$ ./.scripts/granatum.sh --help
```

There are decisions on whether to continue using BASH script or use a binary
compiler for consistency and portability.

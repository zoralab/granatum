<!--
+++
date = "2020-01-16T22:08:04+08:00"
title = "FW-ATI Profile"
description = """
Granatum 'fw-ati' profile explains its simple-cdd's role and technical
specifications.
"""
keywords = ["fw-ati", "granatum", "profile"]
authors = ["Granatum Team"]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "B) Profiles"
name = "fw-ati"
weight = 1
+++
-->

# `fw-ati` Profile
`fw-ati` profile is specific [ATI](https://wiki.debian.org/AtiHowTo) GPU
firmware package from `non-free` upstream. This is specifically one firmware
to ATi Graphic Processing Unit.

`fm-ati` depends on `fw-core` and of course, `non free` to be enabled.

It's profile name is known as `fw-ati`.

`fw-ati` is available since `v0.2.0`.



## Inclusion
There are a number of ways depending on how you use Granatum.


### Granatum Templated `.conf` Method
To include `fw-ati` in Granatum templated `.conf` file, add it into
`plist` after adding `fw-core`:

```bash {linenos=table,hl_lines=[4],linenostart=1}
# plist is the list of profiles to be included in this configuration file. It
# ...
# the names are separated by space.
plist="... fw-core fw-ati ..."
```


### Crude Simple-CDD `.conf` Method
If you're using your own `.conf` file, then simply include `fw-core` and
`fw-ati` into both `profiles` and `auto_profiles`:

```bash {linenos=table,hl_lines=[1,2],linenostart=1}
profiles="fw-core fw-ati ..."
auto_profiles="fw-core fw-ati ..."
```




## `fw-ati.packages`
`fw-ati` includes the following packages:
1. `xserver-xorg-video-ati`
2. `libgl1-mesa-dri`

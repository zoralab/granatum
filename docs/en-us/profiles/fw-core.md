<!--
+++
date = "2020-01-16T20:10:39+08:00"
title = "FW-CORE Profile"
description = """
Granatum 'fw-core' profile explains its simple-cdd's role and technical
specifications.
"""
keywords = ["fw-core", "granatum", "profile"]
authors = ["Granatum Team"]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "B) Profiles"
name = "fw-core"
weight = 1
+++
-->

# `fw-core` Profile
`fw-core` profile is the non-free linux firmware (`firmware-linux-nonfree`)
package designed to automatically setup all firmware in a target system.

It is **strictly** depends on `non-free` upstream package list.

It's profile name is known as `fw-core`.

`fw-core` is available since `v0.2.0`.



## Inclusion
There are a number of ways depending on how you use Granatum.

### Granatum Templated `.conf` Method
To include `fw-core` in Granatum templated `.conf` file, add it into
`plist`.

```bash {linenos=table,hl_lines=[4],linenostart=1}
# plist is the list of profiles to be included in this configuration file. It
# ...
# the names are separated by space.
plist="... fw-core ..."
```

### Crude Simple-CDD `.conf` Method
If you're using your own `.conf` file, then simply include `fw-core`
into both `profiles` and `auto_profiles`:

```bash {linenos=table,hl_lines=[1,2],linenostart=1}
profiles="fw-core ..."
auto_profiles="fw-core ..."
```



## `fw-core.packages`
`fw-core` only includes `firmware-linux-nonfree` inside its packages list.

<!--
+++
date = "2020-01-05T17:12:31+08:00"
title = "CUSTOM Profile"
description = """
Granatum 'custom' profile explains its simple-cdd's role and technical
specifications.
"""
keywords = ["custom", "granatum"]
authors = ["Granatum Team"]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "B) Profiles"
name = "custom"
weight = 1
+++
-->

# `custom` Profile
`custom` profile is the user-specific profiles to be included into the ISO
installer. `custom` profile's files are automatically ignored by the workspace
`.gitignore`.

It's profile name is known as `custom`.

`custom` is available since `v0.1.0`.



## Inclusion
There are a number of ways depending on how you use Granatum.

### Granatum Templated `.conf` Method
`custom` is automatically included as the last item in the `plist`. You do
not need to do anything aside creating the `custom.X` profiles.


### Crude Simple-CDD `.conf` Method
If you're using your own `.conf` file, then simply include `custom`
into both `profiles` and `auto_profiles` as the last element:

```bash {linenos=table,hl_lines=[1,2],linenostart=1}
profiles="... custom"
auto_profiles="... custom"
```



## `custom.downloads`
Holds user-specific `profile.donwloads` configurations.




## `custom.excludes`
Holds user-specific `profile.excludes` configurations.




## `custom.packages`
Holds user-specific `profile.packages` configurations.




## `custom.preseed`
Holds user-specific `profile.preseed` configurations.




## `custom.udebs`
Holds user-specific `profile.udebs` configurations.




## `custom.postinst`
Holds user-specific `profile.postinst` configurations.

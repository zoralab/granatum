<!--
+++
date = "2020-01-05T15:52:46+08:00"
title = "BLOADER-LILO Profile"
description = """
Granatum 'bloader-lilo' profile explains its simple-cdd's role and technical
specifications.
"""
keywords = ["bloader-lilo", "granatum"]
authors = ["Granatum Team"]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "B) Profiles"
name = "bloader-lilo"
weight = 1
+++
-->

# `bloader-lilo` Profile
`bloader-lilo` profile is to set the ISO image to use LILO bootloader
instead of the default standard Grub bootloader.

It's profile name is known as `bloader-lilo`.

`bloader-lilo` is available since `v0.1.0`.



## Inclusion
There are a number of ways depending on how you use Granatum.

> **IMPORTANT NOTE**: You can only include 1 type of `bloader-XXXX`. Including
> many bootloaders into your ISO image yields unimaginable consequences.


### Granatum Templated `.conf` Method
To include LILO bootloader installation into your ISO image, you can add
`bloader-lilo` into the `plist`, replacing other bootloaders. It should be
second in place within `plist`. Example:

```bash {linenos=table,hl_lines=[4],linenostart=1}
# plist is the list of profiles to be included in this configuration file. It
# ...
# the names are separated by space.
plist="... bloader-lilo ..."
```

### Crude Simple-CDD `.conf` Method
If you're using your own `.conf` file, then simply include `bloader-lilo`
into both `profiles` and `auto_profiles`:

```bash {linenos=table,hl_lines=[1,2],linenostart=1}
profiles="bloader-lilo ..."
auto_profiles="bloader-lilo ..."
```



## `bloader-lilo.preseed`
`bloader-lilo` only preseeds disabling Grub installation setting in order to
permit its own installation. The settings are:

1. set `true` to skip grub installer for `grub/installer` (since `v0.1.0`)

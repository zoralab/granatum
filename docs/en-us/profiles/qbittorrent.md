<!--
+++
date = "2020-02-15T14:39:07+08:00"
title = "QBITTORRENT Profile"
description = """
Granatum 'qbittorrent' profile explains its simple-cdd's role and technical
specifications.
"""
keywords = ["qbittorrent", "granatum", "profile"]
authors = ["Granatum Team"]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "B) Profiles"
name = "qbittorrent"
weight = 1
+++
-->

# `qbittorrent` Profile
`qbittorrent` profile is to setup qbitorrent client for peer-to-peer file
transferring via torrent.

It's profile name is known as `qbittorrent`.

`qbittorrent` is available since `v0.3.0`.



## Inclusion
There are a number of ways depending on how you use Granatum.

### Granatum Templated `.conf` Method
To include `qbittorrent` in Granatum templated `.conf` file, add it into
`plist`.

```bash {linenos=table,hl_lines=[4],linenostart=1}
# plist is the list of profiles to be included in this configuration file. It
# ...
# the names are separated by space.
plist="... qbittorrent ..."
```

### Crude Simple-CDD `.conf` Method
If you're using your own `.conf` file, then simply include `qbittorrent`
into both `profiles` and `auto_profiles`:

```bash {linenos=table,hl_lines=[1,2],linenostart=1}
profiles="qbittorrent ..."
auto_profiles="qbittorrent ..."
```



## `qbittorrent.packages`
`qbittorrent` install the software package itself.

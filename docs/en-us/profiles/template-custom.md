<!--
+++
date = "2020-01-05T16:51:48+08:00"
title = "TEMPLATE-CUSTOM Profile"
description = """
Granatum 'template-custom' profile explains its simple-cdd's role and technical
specifications.
"""
keywords = ["template-custom", "granatum"]
authors = ["Granatum Team"]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "B) Profiles"
name = "template-custom"
weight = 1
+++
-->

# `template-custom` Profile
`template-custom` profile is the template profiles for writing user-specific
`custom` profiles.

> **IMPORTANT NOTE**: It is not meant for user's usage. Please only reference
> them for writing `custom` profiles.

> **NOTE TO GRANATUM DEVELOPERS AND MAINTAINERS**: Only include the template
> for development and testing. Please remove `template-X` from all `.conf` under
> maintenance.

Its profile name is known as `template-custom`.

`template-custom` is available since `v0.1.0`.



## Inclusion
There are a number of ways depending on how you use Granatum.

### Granatum Templated `.conf` Method
After writing your `custom.XXX` profiles, you do not need to do anything.
`custom` is already included automatically.


### Crude Simple-CDD `.conf` Method
After writing your `custom.XXX` profile, if you're using your own `.conf` file,
then simply include `custom` into both `profiles` and `auto_profiles`:

```bash {linenos=table,hl_lines=[1,2],linenostart=1}
profiles="custom ..."
auto_profiles="custom ..."
```



## `template-custom.downloads`
This file holds all the user-specific inputs for `custom.download`.




## `template-custom.excludes`
This file holds all the user-specific inputs for `custom.excludes`.




## `template-custom.packages`
This file holds all the user-specific inputs for `custom.packages`.




## `template-custom.preseed`
This file holds all the user-specific inputs for `custom.packages`.




## `template-custom.udebs`
This file holds all the user-specific inputs for `custom.udebs`.



## `template-custom.postinst`
This file holds the post-install shell script as a reference to all.

You can duplicate this file to write your profile's `XXXX.postinst`. After
duplication, please update the script's `postinst_name` variable's value to
`custom`. This is for logging purpose.

```sh {linenos=table,hl_lines=[5],linenostart=1}
#!/bin/sh
####################
# SCRIPT VARIABLES #
####################
postinst_name="custom"
log_path="/var/log/granatum_postinst.log"

...
```

You should always write the script as `POSIX SHELL` compliant for maximum
portability and compatibility purposes.

Also, for simplification purpose, write the script as a single executable
file instead of depending on some other scripts or executables.

Keep in mind that `postinst` scripts are executed as if you're using the
installed operating system. It is **NOT** related to the live CD's RAM disk.

For clean codes, please write your shell executions after the
`SCRIPT EXECUTIONS` banner. Example:

```sh {linenos=table,hl_lines=[6],linenostart=1}
...

#####################
# SCRIPT EXECUTIONS #
#####################
1>&2 echo "Write your code after this banner"
```

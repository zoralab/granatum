<!--
+++
date = "2020-01-20T10:01:15+08:00"
title = "WGET Profile"
description = """
Granatum 'wget' profile explains its simple-cdd's role and technical
specifications.
"""
keywords = ["wget", "granatum", "profile"]
authors = ["Granatum Team"]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "B) Profiles"
name = "wget"
weight = 1
+++
-->

# `wget` Profile
`wget` profile is the profile to install `wget` web interfacing software
package.

It's profile name is known as `wget`.

`wget` is available since `v0.3.0`.



## Inclusion
There are a number of ways depending on how you use Granatum.

### Granatum Templated `.conf` Method
To include `wget` in Granatum templated `.conf` file, add it into
`plist`.

```bash {linenos=table,hl_lines=[4],linenostart=1}
# plist is the list of profiles to be included in this configuration file. It
# ...
# the names are separated by space.
plist="... wget ..."
```

### Crude Simple-CDD `.conf` Method
If you're using your own `.conf` file, then simply include `wget`
into both `profiles` and `auto_profiles`:

```bash {linenos=table,hl_lines=[1,2],linenostart=1}
profiles="wget ..."
auto_profiles="wget ..."
```



## `wget.packages`
`wget` only install the following packages:

1. `wget`

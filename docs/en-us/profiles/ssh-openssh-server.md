<!--
+++
date = "2020-01-17T11:33:33+08:00"
title = "SSH-OPENSSH-SERVER Profile"
description = """
Granatum 'ssh-openssh-server' profile explains its simple-cdd's role and
technical specifications.
"""
keywords = ["ssh-openssh-server", "granatum", "profile"]
authors = ["Granatum Team"]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "B) Profiles"
name = "ssh-openssh-server"
weight = 1
+++
-->

# `ssh-openssh-server` Profile
`ssh-openssh-server` profile is the openSSH server package for others to access
this machine remotely.

It's profile name is known as `ssh-openssh-server`.

`ssh-openssh-server` is available since `v0.2.0`.



## Inclusion
There are a number of ways depending on how you use Granatum.

### Granatum Templated `.conf` Method
To include `ssh-openssh-server` in Granatum templated `.conf` file, add it into
`plist`.

```bash {linenos=table,hl_lines=[4],linenostart=1}
# plist is the list of profiles to be included in this configuration file. It
# ...
# the names are separated by space.
plist="... ssh-openssh-server ..."
```

### Crude Simple-CDD `.conf` Method
If you're using your own `.conf` file, then simply include `ssh-openssh-server`
into both `profiles` and `auto_profiles`:

```bash {linenos=table,hl_lines=[1,2],linenostart=1}
profiles="ssh-openssh-server ..."
auto_profiles="ssh-openssh-server ..."
```



## `ssh-openssh-server.packages`
`ssh-openssh-server` includes the following packages:

1. `openssh-server`




## `ssh-openssh-server.postinst`
To ensure SSH server not allowing root account login, although by default it
is disabled, this `postinst` will forcefully set it to `no`.

<!--
+++
date = "2020-02-02T10:12:17+08:00"
title = "X-TOUCHPAD-TAPPING Profile"
description = """
Granatum 'x-touchpad-tapping' profile explains its simple-cdd's role and
technical specifications.
"""
keywords = ["x-touchpad-tapping", "granatum", "profile"]
authors = ["Granatum Team"]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "B) Profiles"
name = "x-touchpad-tapping"
weight = 1
+++
-->

# `x-touchpad-tapping` Profile
`x-touchpad-tapping` profile is that set certain touchpad to have tapping
functions on X11 desktop interface. Obviously, this profile depends on X11
existence via various profiles like `sddm` or `gdm`.

It's profile name is known as `x-touchpad-tapping`.

`x-touchpad-tapping` is available since `v0.3.0`.



## Inclusion
There are a number of ways depending on how you use Granatum.

### Granatum Templated `.conf` Method
To include `x-touchpad-tapping` in Granatum templated `.conf` file, add it into
`plist`.

```bash {linenos=table,hl_lines=[4],linenostart=1}
# plist is the list of profiles to be included in this configuration file. It
# ...
# the names are separated by space.
plist="... x-touchpad-tapping ..."
```

### Crude Simple-CDD `.conf` Method
If you're using your own `.conf` file, then simply include `x-touchpad-tapping`
into both `profiles` and `auto_profiles`:

```bash {linenos=table,hl_lines=[1,2],linenostart=1}
profiles="x-touchpad-tapping ..."
auto_profiles="x-touchpad-tapping ..."
```



## `x-touchpad-tapping.postinst`
`x-touchpad-tapping` first check if `/etc/X11` is installed in the system.
Otherwise, it bails out immediately as `X11` is not setup properly.

If all is good, `x-touchpad-tapping` creates a configuration file inside
`/etc/X11/xorg.conf.d/` named `40-libinput.conf`. It also creates the directory
regardlessly if `x11/xorg.conf.d` directory is missing by default. The content
of the file is as follows:

```bash {linenos=table,linenostart=1}
Section "InputClass"
Identifier "libinput touchpad catchall"
MatchIsTouchpad "on"
MatchDevicePath "/dev/input/event*"
Driver "libinput"
Option "Tapping" "on"
EndSection
```

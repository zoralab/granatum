<!--
+++
date = "2020-01-05T10:48:06+08:00"
title = "CORE Profile"
description = """
Granatum 'core' profile explains its simple-cdd's role and technical
specifications.
"""
keywords = ["core", "granatum"]
authors = ["Granatum Team"]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "B) Profiles"
name = "core"
weight = 1
+++
-->

# `core` Profile
`core` profile is the main profile for Granatum build ISO image. It is the main
components for inclusion to create a smooth Debian install experience under
Granatum controls.

It's profile name is known as `core`.

`core` is available since `v0.1.0`.


## Inclusion
There are a number of ways depending on how you use Granatum.


### Granatum Templated `.conf` Method
`core` is automatically included in the available `.conf`
**"AUTOMATION - profiling CD"** section. There is no opt-out option for `core`.

It is populated as the first profile in the `plist` regardless of user input.


### Crude Simple-CDD `.conf` Method
If you're using your own .`conf` file, then simply include `core` into both
`profiles` and `auto_profiles` as the **FIRST** profile:

```bash {linenos=table,hl_lines=[1,2],linenostart=1}
profiles="core ..."
auto_profiles="core ..."
```


## `core.preseed`
`core` preseeds a number of settings as default values.


### Upstream Configurations (Apt, Mirror)
`core` preseed a large number `apt` related installation values such as:

1. to use `non-free` upstream for apt-setup.
2. to use `contrib` upstream for apt-setup.
3. to include `security`, and `update` into apt services for apt-setup.
4. set security host to `security.debian.org` for apt-setup.
5. set http directory to `/debian` for apt mirror.
6. clear http proxy to empty string for apt mirror.
7. set install recommended packages for base-installer.


### Installer Network
`core` currently preseed installer's network configurations values to:

1. set `automatic` for network configurations interface.

> **NOTE:** Wifi network connectivity automation is still in progress.


### Popularity Contest
`core` disables popularity contest by default.

> **RATIONALE:** This allows user to explicitly enable the contest during
> OS usage as a mean to get user consent before performing data collections.


### Scan CD
`core` disables scan CD during installation with:

1. set `set-first` to false for `apt-setup/cdrom`.
2. set `set-next` to false for `apt-setup/cdrom`.
3. set `set-failed` to false for `apt-setup/cdrom`.

The rationale is that Granatum must build the percise installer ISO image with
all customizations included under 1 image, not scattered across multiple
mediums and confuses the end-users.


## `core.udebs`
These are the default micro-debs included in `core` profile.

### All Available Firmware Packages
To ensure a smooth network install especially facilitating wifi network
connectivity, `core` includes all available network firmwares. They are:

1. `firmware-realtek` (since `v0.1.0`)
2. `firmware-atheros` (since `v0.1.0`)
3. `firmware-brcm80211` (since `v0.1.0`)
4. `firmware-ipw2x00` (since `v0.1.0`)
5. `firmware-iwlwifi` (since `v0.1.0`)
6. `firmware-linux` (since `v0.1.0`)
7. `firmware-ralink` (since `v0.1.0`)
8. `firmware-ti-connectivity` (since `v0.1.0`)

<!--
+++
date = "2020-01-07T19:34:21+08:00"
title = "BACKPORTS-CORE Profile"
description = """
Granatum 'backports-core' profile explains its simple-cdd's role and technical
specifications.
"""
keywords = ["backports-core", "profile", "granatum"]
authors = ["Granatum Team"]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "B) Profiles"
name = "backports-core"
weight = 1
+++
-->

# `backports-core` Profile
`backports-core` profile is the configurations to add backports upstream into
the installed operating system. It is the backports' core module where you
need it to build other backport profiles.

It's profile name is known as `backports-core`.

`backports-core` is available since `v0.1.0`.



## Inclusion
There are a number of ways depending on how you use Granatum.

### Granatum Templated `.conf` Method
You may include `backports-core` into the `plist`. The position should be
**FIRST** among all backports. Otherwise, your other backports will fail with
unimaginable concequences. Example:

```bash {linenos=table,hl_lines=[4],linenostart=1}
# plist is the list of profiles to be included in this configuration file. It
# ...
# the names are separated by space.
plist="... backport-cores backport-X backport-Y ..."
```

### Crude Simple-CDD `.conf` Method
If you're using your own `.conf` file, then simply include `backport-core`
into both `profiles` and `auto_profiles` as the **FIRST** among all backports:

```bash {linenos=table,hl_lines=[1,2],linenostart=1}
profiles="backports-core backports-X backports-Y ..."
auto_profiles="backports-core backports-X backports-Y ..."
```



## `backports-core.postinst`
`backports-core` uses a POSIX SHELL complaint script to parse
`/etc/apt/sources.list` for input and generate the
`/etc/apt/sources.list.d/backports.list` output. It has the capability to parse
the upstream `codename`, `url`, and the `releases` and smartly generate the
backports statements.

The program will call `apt-get update` so an internet connection is needed.

This facilitates timeless creation and low maintenance.

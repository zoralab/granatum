<!--
+++
date = "2020-02-15T15:20:47+08:00"
title = "RESOLVCONF Profile"
description = """
Granatum 'resolvconf' profile explains its simple-cdd's role and technical
specifications.
"""
keywords = ["resolvconf", "granatum", "profile"]
authors = ["Granatum Team"]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "B) Profiles"
name = "resolvconf"
weight = 1
+++
-->

# `resolvconf` Profile
`resolvconf` profile is the DNS resolver configuration software to facilitate
custom DNS resolutions.

It's profile name is known as `resolvconf`.

`resolvconf` is available since `v0.3.0`.



## Inclusion
There are a number of ways depending on how you use Granatum.

### Granatum Templated `.conf` Method
To include `resolvconf` in Granatum templated `.conf` file, add it into
`plist`.

```bash {linenos=table,hl_lines=[4],linenostart=1}
# plist is the list of profiles to be included in this configuration file. It
# ...
# the names are separated by space.
plist="... resolvconf ..."
```

### Crude Simple-CDD `.conf` Method
If you're using your own `.conf` file, then simply include `resolvconf`
into both `profiles` and `auto_profiles`:

```bash {linenos=table,hl_lines=[1,2],linenostart=1}
profiles="resolvconf ..."
auto_profiles="resolvconf ..."
```



## `resolvconf.packages`
`resolveconf` only install the software package itself.

<!--
+++
date = "2020-01-05T16:37:25+08:00"
title = "UFW Profile"
description = """
Granatum 'ufw' profile explains its simple-cdd's role and technical
specifications.
"""
keywords = ["ufw", "granatum"]
authors = ["Granatum Team"]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "B) Profiles"
name = "ufw"
weight = 1
+++
-->

# `ufw` Profile
`ufw` profile is the software package `ufw` to be installed into operating
system.

It's profile name is known as `ufw`.

`ufw` is available since `v0.1.0`.



## Inclusion
There are a number of ways depending on how you use Granatum.

### Granatum Templated `.conf` Method
To include `ufw` software package, simply add `ufw` into `plist`. Example:

```bash {linenos=table,hl_lines=[4],linenostart=1}
# plist is the list of profiles to be included in this configuration file. It
# ...
# the names are separated by space.
plist="... ufw ..."
```

### Crude Simple-CDD `.conf` Method
If you're using your own `.conf` file, then simply include `ufw`
into both `profiles` and `auto_profiles`:

```bash {linenos=table,hl_lines=[1,2],linenostart=1}
profiles="ufw ..."
auto_profiles="ufw ..."
```



## `ufw.packages`
`ufw` only includes its own inside the post-install packages. The final
operating system should have the `ufw` application available system-wide.

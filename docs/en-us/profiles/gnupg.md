<!--
+++
date = "2020-01-15T21:41:36+08:00"
title = "GNUPG Profile"
description = """
Granatum 'gnupg' profile explains its simple-cdd's role and technical
specifications.
"""
keywords = ["gnupg", "granatum", "profile"]
authors = ["Granatum Team"]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "B) Profiles"
name = "gnupg"
weight = 1
+++
-->

# `gnupg` Profile
`gnupg` profile is the GnuPG software package for privacy enhanced key
management.

It's profile name is known as `gnupg`.

`gnupg` is available since `v0.2.0`.



## Inclusion
There are a number of ways depending on how you use Granatum.

### Granatum Templated `.conf` Method
To include `gnupg` in Granatum templated `.conf` file, add it into
`plist`.

```bash {linenos=table,hl_lines=[4],linenostart=1}
# plist is the list of profiles to be included in this configuration file. It
# ...
# the names are separated by space.
plist="... gnupg ..."
```

### Crude Simple-CDD `.conf` Method
If you're using your own `.conf` file, then simply include `gnupg`
into both `profiles` and `auto_profiles`:

```bash {linenos=table,hl_lines=[1,2],linenostart=1}
profiles="gnupg ..."
auto_profiles="gnupg ..."
```



## `gnupg.packages`
`gnupg` only includes itself as a software package for installation.

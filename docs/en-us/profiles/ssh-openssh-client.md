<!--
+++
date = "2020-01-17T11:26:30+08:00"
title = "SSH-OPENSSH-CLIENT Profile"
description = """
Granatum 'ssh-openssh-client' profile explains its simple-cdd's role and
technical specifications.
"""
keywords = ["ssh-openssh-client", "granatum", "profile"]
authors = ["Granatum Team"]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "B) Profiles"
name = "ssh-openssh-client"
weight = 1
+++
-->

# `ssh-openssh-client` Profile
`ssh-openssh-client` profile is open Secure Shell (openSSH) client interface
software package.

It's profile name is known as `ssh-openssh-client`.

`ssh-openssh-client` is available since `v0.2.0`.



## Inclusion
There are a number of ways depending on how you use Granatum.

### Granatum Templated `.conf` Method
To include `ssh-openssh-client` in Granatum templated `.conf` file, add it into
`plist`.

```bash {linenos=table,hl_lines=[4],linenostart=1}
# plist is the list of profiles to be included in this configuration file. It
# ...
# the names are separated by space.
plist="... ssh-openssh-client ..."
```

### Crude Simple-CDD `.conf` Method
If you're using your own `.conf` file, then simply include `ssh-openssh-client`
into both `profiles` and `auto_profiles`:

```bash {linenos=table,hl_lines=[1,2],linenostart=1}
profiles="ssh-openssh-client ..."
auto_profiles="ssh-openssh-client ..."
```



## `ssh-openssh-client.packages`
`ssh-openssh-client` only includes the following packages:

1. `openssh-client`

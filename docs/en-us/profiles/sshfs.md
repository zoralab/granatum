<!--
+++
date = "2020-01-08T21:57:20+08:00"
title = "SSHFS Profile"
description = """
Granatum 'sshfs' profile explains its simple-cdd's role and technical
specifications.
"""
keywords = ["sshfs", "granatum", "profile"]
authors = ["Granatum Team"]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "B) Profiles"
name = "sshfs"
weight = 1
+++
-->

# `sshfs` Profile
`sshfs` profile is the `sshfs` package to be included in the installed
operating system.

It's profile name is known as `sshfs`.

`sshfs` is available since `v0.2.0`.



## Inclusion
There are a number of ways depending on how you use Granatum.

### Granatum Templated `.conf` Method
To include `sshfs` in Granatum templated `.conf` file, add it into
`plist`.

```bash {linenos=table,hl_lines=[4],linenostart=1}
# plist is the list of profiles to be included in this configuration file. It
# ...
# the names are separated by space.
plist="... sshfs ..."
```

### Crude Simple-CDD `.conf` Method
If you're using your own `.conf` file, then simply include `sshfs`
into both `profiles` and `auto_profiles`:

```bash {linenos=table,hl_lines=[1,2],linenostart=1}
profiles="sshfs ..."
auto_profiles="sshfs ..."
```



## `sshfs.packages`
`sshfs` only includes its own inside the post-install packages. The final
operating system should have `sshfs` setup and ready to use system-wide.

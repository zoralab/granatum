<!--
+++
date = "2020-01-06T09:43:01+08:00"
title = "HAVEGED Profile"
description = """
Granatum 'haveged' profile explains its simple-cdd's role and technical
specifications.
"""
keywords = ["haveged", "granatum"]
authors = ["Granatum Team"]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "B) Profiles"
name = "haveged"
weight = 1
+++
-->

# `haveged` Profile
`haveged` profile is the software package to be installed into the system.

It's profile name is known as `haveged`.

`haveged` is available since `v0.1.0`.



## Inclusion
There are a number of ways depending on how you use Granatum.

### Granatum Templated `.conf` Method
Explain how to include the profile using granatum templated `.conf` file  into
this section.

```bash {linenos=table,hl_lines=[4],linenostart=1}
# plist is the list of profiles to be included in this configuration file. It
# ...
# the names are separated by space.
plist="... haveged ..."
```

### Crude Simple-CDD `.conf` Method
If you're using your own `.conf` file, then simply include `haveged`
into both `profiles` and `auto_profiles`:

```bash {linenos=table,hl_lines=[1,2],linenostart=1}
profiles="haveged ..."
auto_profiles="haveged ..."
```



## `haveged.packages`
`haveged` only includes its own inside the post-install packages. The final
operating system should have the `haveged` setup and ready to use system-wide.

<!--
+++
date = "2020-01-15T08:21:18+08:00"
title = "PLYMOUTH-CORE Profile"
description = """
Granatum 'plymouth-core' profile explains its simple-cdd's role and technical
specifications.
"""
keywords = ["plymouth-core", "granatum", "profile"]
authors = ["Granatum Team"]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "B) Profiles"
name = "plymouth-core"
weight = 1
+++
-->

# `plymouth-core` Profile
`plymouth-core` profile is the bootloading graphic animator that stylize the
boot screen is available at: https://wiki.debian.org/plymouth. `plymouth-core`
depends on `bloader-grub` in order to work properly. Anything outside of this
range are untested and can yield unimaginable circumstances.

It's profile name is known as `plymouth-core`.

`plymouth-core` is available since `v0.2.0`.



## Inclusion
There are a number of ways depending on how you use Granatum.

### Granatum Templated `.conf` Method
To include `plymouth-core` in Granatum templated `.conf` file, add it into
`plist`.

```bash {linenos=table,hl_lines=[4],linenostart=1}
# plist is the list of profiles to be included in this configuration file. It
# ...
# the names are separated by space.
plist="... plymouth-core ..."
```

### Crude Simple-CDD `.conf` Method
If you're using your own `.conf` file, then simply include `plymouth-core`
into both `profiles` and `auto_profiles`:

```bash {linenos=table,hl_lines=[1,2],linenostart=1}
profiles="plymouth-core ..."
auto_profiles="plymouth-core ..."
```



## `plymouth-core.packages`
`plymouth-core` only includes the following packages:

1. `plymouth` - the executable binary
2. `plymouth-themes` - the default themes needed by `plymouth`.




## `plymouth-core.postinst`
`plymouth-core` executes the configurations needed to be done during
post-install as specified in https://wiki.debian.org/plymouth. Among the
tasks being done are:

1. Intelligently detect graphic driver module used in the machine.
2. Adding detected graphic driver module to `/etc/initramfs-tools/modules`.
3. Adding `drm` driver module into `/etc/initramfs-tools/modules`.
4. set `GRUB_GFXMODE` to use default display dimension or fallback to
   default `auto`.
5. set `GRUB_CMDLINE_LINUX_DEFAULT` to use splash screen.
6. update grub to match settings.
7. set default theme to `solar` and recompile initramfs.

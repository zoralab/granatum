<!--
+++
date = "2020-01-05T15:26:07+08:00"
title = "BLOADER-GRUB Profile"
description = """
Granatum 'bloader-grub' profile explains its simple-cdd's role and technical
specifications.
"""
keywords = ["bloader-grub", "granatum"]
authors = ["Granatum Team"]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "B) Profiles"
name = "bloader-grub"
weight = 1
+++
-->

# `bloader-grub` Profile
`bloader-grub` is to set the ISO image to use Grub bootloader. It is the default
standard bootloader shipped by Debian.

It's profile name is known as `bloader-grub`.

`bloader-grub` is available since `v0.1.0`.



## Inclusion
There are a number of ways depending on how you use Granatum.

> **IMPORTANT NOTE**: You can only include 1 type of `bloader-XXXX`. Including
> many bootloaders into your ISO image yields unimaginable consequences.


### USER INPUT: `custom.preseed`
There are a number of user input settings required to be inside your
`custom.preseed`. See below for detailed info.

Without the `custom.preseed`, headless installation experience will break by
prompting you to fill in those missing output.


### Granatum Templated `.conf` Method
`bloader-grub` is included as the default bootloader profile. It should
arrive as second in the `plist`.

```bash {linenos=table,hl_lines=[4],linenostart=1}
# plist is the list of profiles to be included in this configuration file. It
# ...
# the names are separated by space.
plist="... bloader-grub ..."
```

### Crude Simple-CDD `.conf` Method
If you're using your own `.conf` file, then simply include `bloader-grub`
into both `profiles` and `auto_profiles`:

```bash {linenos=table,hl_lines=[1,2],linenostart=1}
profiles="bloader-grub ..."
auto_profiles="bloader-grub ..."
```




## `bloader-grub.preseed`
`bloader-grub` preseed all the safety values for its bootloader installation.
They are:

1. Install grub automatically if the computer has only Debian for
`grub/installer` (since `v0.1.0`)
2. Install grub automatically if the computer has other OS for
`grub/intaller` (since `v0.1.0`)


## `custom.preseed`
Grub requires some user inputs to maintain its headless installation experience.


### Set Bootloader Installation Target
Grub needs user's input to specify target disk to install the bootloader. Hence,
you need to include the following preseed into your `custom.preseed` in order to
maintain the headless installation experience. See `template-custom.preseed`'s
**'BOOTLOADER'** section for code examples.

```bash
# set Grub bootloader's target disk.
# Optional if 'lilo' or 'nobootloader' is chosen
d-i grub-installer/bootdev  string /dev/to/your/disk
```

<!--
+++
date = "2020-01-31T19:13:25+08:00"
title = "SDDM-AUTOLOGIN Profile"
description = """
Granatum 'sddm-autologin' profile explains its simple-cdd's role and technical
specifications.
"""
keywords = ["sddm", "sddm-autologin", "granatum", "profile"]
authors = ["Granatum Team"]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "B) Profiles"
name = "sddm-autologin"
weight = 1
+++
-->

# `sddm-autologin` Profile
`sddm-autologin` profile is the auto-login configuration for `sddm` desktop
manager. It depends on `sddm` to be installed via various other profiles
including it.

It's profile name is known as `sddm-autologin`.

`sddm-autologin` is available since `v0.3.0`.



## Inclusion
There are a number of ways depending on how you use Granatum.

### Granatum Templated `.conf` Method
To include `sddm-autologin` in Granatum templated `.conf` file, add it into
`plist`.

```bash {linenos=table,hl_lines=[4],linenostart=1}
# plist is the list of profiles to be included in this configuration file. It
# ...
# the names are separated by space.
plist="... sddm-autologin ..."
```

### Crude Simple-CDD `.conf` Method
If you're using your own `.conf` file, then simply include `sddm-autologin`
into both `profiles` and `auto_profiles`:

```bash {linenos=table,hl_lines=[1,2],linenostart=1}
profiles="sddm-autologin ..."
auto_profiles="sddm-autologin ..."
```



## `sddm-autologin.postinst`
`sddm-autologin` scans the `/usr/share/xsessions/*.desktop` for the first
choice of desktop manager and `/home` for the first user choice.

With these 2 inputs in placed, the script then create a
`/etc/sddm.conf.d/autologin.conf` by setting the parsed user and desktop
manager names.

If any error found in between the process, the script will bail out and the
setup will fail without detering the installation progress.

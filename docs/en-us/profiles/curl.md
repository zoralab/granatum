<!--
+++
date = "2020-01-20T10:42:59+08:00"
title = "CURL Profile"
description = """
Granatum 'curl' profile explains its simple-cdd's role and technical
specifications.
"""
keywords = ["curl", "granatum", "profile"]
authors = ["Granatum Team"]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "B) Profiles"
name = "curl"
weight = 1
+++
-->

# `curl` Profile
`curl` profile is the `curl` software package for communicating with network.

It's profile name is known as `curl`.

`curl` is available since `v0.3.0`.



## Inclusion
There are a number of ways depending on how you use Granatum.

### Granatum Templated `.conf` Method
To include `curl` in Granatum templated `.conf` file, add it into
`plist`.

```bash {linenos=table,hl_lines=[4],linenostart=1}
# plist is the list of profiles to be included in this configuration file. It
# ...
# the names are separated by space.
plist="... curl ..."
```

### Crude Simple-CDD `.conf` Method
If you're using your own `.conf` file, then simply include `curl`
into both `profiles` and `auto_profiles`:

```bash {linenos=table,hl_lines=[1,2],linenostart=1}
profiles="curl ..."
auto_profiles="curl ..."
```



## `curl.packages`
`curl` only include the following packages:

1. `curl`

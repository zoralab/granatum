<!--
+++
date = "2020-01-16T18:53:56+08:00"
title = "VIM Profile"
description = """
Granatum 'vim' profile explains its simple-cdd's role and technical
specifications.
"""
keywords = ["vim", "granatum", "profile"]
authors = ["Granatum Team"]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "B) Profiles"
name = "vim"
weight = 1
+++
-->

# `vim` Profile
`vim` profile is the VIM software package for installing into the base system.

It's profile name is known as `vim`.

`vim` is available since `v0.2.0`.



## Inclusion
There are a number of ways depending on how you use Granatum.

### Granatum Templated `.conf` Method
To include `vim` in Granatum templated `.conf` file, add it into
`plist`.

```bash {linenos=table,hl_lines=[4],linenostart=1}
# plist is the list of profiles to be included in this configuration file. It
# ...
# the names are separated by space.
plist="... vim ..."
```

### Crude Simple-CDD `.conf` Method
If you're using your own `.conf` file, then simply include `vim`
into both `profiles` and `auto_profiles`:

```bash {linenos=table,hl_lines=[1,2],linenostart=1}
profiles="vim ..."
auto_profiles="vim ..."
```



## `vim.packages`
`vim` only installs itself inside the `vim.packages` file.




## `vim.postinst`
`vim` pruges with `--auto-remove` the `vim-tiny` package to ensure there is
only 1 type of vim in the target system.

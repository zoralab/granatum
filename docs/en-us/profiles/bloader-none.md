<!--
+++
date = "2020-01-05T16:00:29+08:00"
title = "BLOADER-NONE Profile"
description = """
Granatum 'bloader-none' profile explains its simple-cdd's role and technical
specifications.
"""
keywords = ["bloader-none", "granatum"]
authors = ["Granatum Team"]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "B) Profiles"
name = "bloader-none"
weight = 1
+++
-->

# `bloader-none` Profile
`bloader-none` profile is to disable bootloader installation.

It's profile name is known as `bloader-none`.

`bloader-none` is available since `v0.1.0`.



## Inclusion
There are a number of ways depending on how you use Granatum.

> **IMPORTANT NOTE**: You can only include 1 type of `bloader-XXXX`. Including
> many bootloaders into your ISO image yields unimaginable consequences.


### Granatum Templated `.conf` Method
To exclude all bootloader installations, you can add `bloader-none` into the
`plist`, replacing other bootloader profiles. It should be in second place.
Example:

```bash {linenos=table,hl_lines=[4],linenostart=1}
# plist is the list of profiles to be included in this configuration file. It
# ...
# the names are separated by space.
plist="... bloader-none ..."
```

### Crude Simple-CDD `.conf` Method
If you're using your own `.conf` file, then simply include `bloader-none`
into both `profiles` and `auto_profiles`:

```bash {linenos=table,hl_lines=[1,2],linenostart=1}
profiles="bloader-none ..."
auto_profiles="bloader-none ..."
```



## `bloader-none.preseed`
`bloader-none` preseeds bootloaders settings to ensure no bootloader is
installed. They are:

1. set to skip grub installation for `grub-installer` (since `v0.1.0`)
2. set to skip lilo installation for `lilo-installer` (since `v0.1.0`)

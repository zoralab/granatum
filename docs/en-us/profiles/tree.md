<!--
+++
date = "2020-01-05T16:28:52+08:00"
title = "TREE Profile"
description = """
Granatum 'tree' profile explains its simple-cdd's role and technical
specifications.
"""
keywords = ["tree", "granatum"]
authors = ["Granatum Team"]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "B) Profiles"
name = "tree"
weight = 1
+++
-->

# `tree` Profile
`tree` profile is the software package to be installed into the operating
system.

It's profile name is known as `tree`.

`tree` is available since `v0.1.0`.



## Inclusion
There are a number of ways depending on how you use Granatum.

### Granatum Templated `.conf` Method
To include `tree` package into the software installation, simply add `tree`
into the `plist`. Example:

```bash {linenos=table,hl_lines=[4],linenostart=1}
# plist is the list of profiles to be included in this configuration file. It
# ...
# the names are separated by space.
plist="... tree ..."
```

### Crude Simple-CDD `.conf` Method
If you're using your own `.conf` file, then simply include `tree`
into both `profiles` and `auto_profiles`:

```bash {linenos=table,hl_lines=[1,2],linenostart=1}
profiles="tree ..."
auto_profiles="tree ..."
```



## `tree.packages`
`tree` only includes its own inside the post-install packages. The final
operating system should have the `tree` application available system-wide.

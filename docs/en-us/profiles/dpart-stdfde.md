<!--
+++
date = "2020-01-05T14:31:55+08:00"
title = "DPART-STDFDE Profile"
description = """
Granatum 'dpart-stdfde' profile explains its simple-cdd's role and technical
specifications.
"""
keywords = ["dpart-stdfde", "granatum"]
authors = ["Granatum Team"]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "B) Profiles"
name = "dpart-stdfde"
weight = 1
+++
-->

# `dpart-stdfde` Profile
`dpart-stdfde` profile is the disk partition profile using standard full disk
encryption provided by Debian.

It's profile name is known as `dpart-stdfde`.

`dpart-stdfde` is available since `v0.1.0`.



## Inclusion
There are a number of ways depending on how you use Granatum.

### USER INPUT: `custom.preseed`
There are settings required to be set inside `custom.preseed`. See below for
detailed info.

Without the `custom.preseed`, headless installation experience will break by
prompting you to fill in those missing input.


### Granatum Templated `.conf` Method
`dpart-stdfde` is included as default option to partition your entire disk with
encrypted partition. It should always arrive as first in the `plist`.

```bash {linenos=table,hl_lines=[4],linenostart=1}
# plist is the list of profiles to be included in this configuration file. It
# ...
# the names are separated by space.
plist="dpart-stdfde ..."
```

### Crude Simple-CDD `.conf` Method
If you're using your own `.conf` file, then simply include `dpart-stdfde` and
`custom` (in sequence) into both `profiles` and `auto_profiles`:

```bash {linenos=table,hl_lines=[1,2],linenostart=1}
profiles="dpart-stdfde custom ..."
auto_profiles="dpart-stdfde custom ..."
```



## `dpart-stdfde.preseed`
`dpart-stdfde` preseeds a large number of settings thanks to the options
available from the `partman` installer.


### Terminate Installer Guide
1. terminate auto-partition selection with
`Guided - use entire disk and set up encrypted LVM` for
`partman-auto/init_automatically_partition` (since `v0.1.0`)
2. terminate choose recipe with
`All files in one partition (recommended for new users)` for
`partman-auto/choose_recipe`(since `v0.1.0`)


### Select Method to Use
1. Set `crypto` for `partman-auto/method` (since `v0.1.0`)


### Eliminate LVM and RAID Prompts
1. set `true` for `partman-lvm/device_remove_lvm` (since `v0.1.0`)
2. set `true` for `partman-md/device_remove_md` (since `v0.1.0`)
3. set `true` for `partman-lvm/device_remove_lvm_span` (since `v0.1.0`)
4. set `true` for `partman-lvm/confim` (since `v0.1.0`)
5. set `true` for `partman-lvm/confirm_nooverwrite` (since `v0.1.0`)
6. set `true` for `partman-lvm/purge_lvm_from_device` (since `v0.1.0`)
7. set `true` for `partman-auto/purge_lvm_from_device` (since `v0.1.0`)
8. set `system` for `partman-auto-lvm/new_vg_name` volume group name
(since `v0.1.0`)


### Write Disk Confirmation
1. set `true` for `partman/confirm_write_new_label` (since `v0.1.0`)
2. set `finish` for `partman/choose_partition` (since `v0.1.0`)
3. set `true` for `partman/confirm_nooverwrite` (since `v0.1.0`)
4. set `true` for `partman/confirm` (since `v0.1.0`)


### Eliminate Force UEFI Prompts
1. set `true` for `d-i partman-efi/non_efi_system` (since `v0.2.0`)


### Eliminate SWAP Prompts
1. set `true` for `d-i partman-basicfilesystems/no_swap` (since `v0.2.0`)


### Set GPT Partition Table
The partition table is set to GPT instead of MBR (since `v0.2.0`)

```
d-i partman-basicfilesystems/choose_label string gpt
d-i partman-basicfilesystems/default_label string gpt
d-i partman-partitioning/choose_label string gpt
d-i partman-partitioning/default_label string gpt
d-i partman/choose_label string gpt
d-i partman/default_label string gpt
```


### Expert Partition Scheme
An expert partition recipe was added for consistency between devices and restore
"Expert Install" functionality. (since `v0.2.0`)

```
# partition scheme
d-i partman-auto-lvm/new_vg_name string cerebrum_crypt
d-i partman-auto/choose_recipe select dpart-stdfde
d-i partman-auto/expert_recipe string                         \
      dpart-stdfde ::                                         \
              1 1 1 free                                      \
                       $bios_boot{ }                          \
                       method{ biosgrub }                     \
              538 538 538 free                                \
                       $primary{ }                            \
                       $iflabel{ gpt }                        \
                       $lvmignore{ }                          \
                       method{ efi } format{ }                \
              .                                               \
              500 500 500 ext3                                \
                      $primary{ } $bootable{ }                \
                      method{ format } format{ }              \
                      use_filesystem{ } filesystem{ ext4 }    \
                      mountpoint{ /boot }                     \
              .                                               \
              100% 100% 100% linux-swap                       \
                      $lvmok{ } lv_name{ swap }               \
                      $primary{ }                             \
                      method{ swap } format{ }                \
                      in_vg { cerebrum_crypt }                \
              .                                               \
              1000 10000 -1 ext4                              \
                      $lvmok{ } lv_name{ root }               \
                      $primary{ }                             \
                      method{ format } format{ }              \
                      use_filesystem{ } filesystem{ ext4 }    \
                      mountpoint{ / }                         \
                      in_vg { cerebrum_crypt }                \
              .
```




## `custom.preseed`
`dpart-stdfde` relies on some user input preseed data in `custom.preseed`.
Hence, you need to include the following preseed in order to maintain the
headless installation experience. See `template-custom.preseed`
**'PARTITIONING SETTINGS'** section for code examples.

### Set Partitioning Target Disk
You **MUST** provide the target disk location:
```bash
# set partition target disk
d-i partman-auto/disk string /dev/to/your/disk
```

### Set Partitioning Sizing Guide
You **MUST** provide the sizing guide:
```bash
# set partition sizing guide (max, min, percent, 100G, etc)
d-i partman-auto-lvm/guided_size string max
```

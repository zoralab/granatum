<!--
+++
date = "2020-01-07T14:08:17+08:00"
title = "DESKENV-LXQT-SDDM Profile"
description = """
Granatum 'deskenv-lxqt-sddm' profile explains its simple-cdd's role and
technical specifications.
"""
keywords = ["deskenv-lxqt-sddm", "profile", "granatum"]
authors = ["Granatum Team"]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "B) Profiles"
name = "deskenv-lxqt-sddm"
weight = 1
+++
-->

# `deskenv-lxqt-sddm` Profile
`deskenv-lxqt-sddm` profile is the [LXQt](https://wiki.debian.org/LXQt) desktop
environment with [SDDM](https://wiki.debian.org/SDDM) desktop manager. This
will turn the minimal install Debian OS into a desktop/laptop with LXQt
graphicial interface.

It's profile name is known as `deskenv-lxqt-sddm`.

> **NOTE**: `lxqt` and `sddm` are both installed during post-installation stage
> where there is no frontend indication of working. To ensure it is installing,
> simply press `CTRL+ALT+F4` to switch to debug console mode and watch its
> installation status. Otherwise, you can wait for its long installation time.

`deskenv-lxqt-sddm` is available since `v0.1.0`.



## Inclusion
There are a number of ways depending on how you use Granatum.

### Granatum Templated `.conf` Method
To include `deskenv-lxqt-sddm` profile, simply add it into the `plist`.

```bash {linenos=table,hl_lines=[4],linenostart=1}
# plist is the list of profiles to be included in this configuration file. It
# ...
# the names are separated by space.
plist="... deskenv-lxqt-sddm ..."
```

### Crude Simple-CDD `.conf` Method
If you're using your own `.conf` file, then simply include `deskenv-lxqt-sddm`
into both `profiles` and `auto_profiles`:

```bash {linenos=table,hl_lines=[1,2],linenostart=1}
profiles="deskenv-lxqt-sddm ..."
auto_profiles="deskenv-lxqt-sddm ..."
```



## `deskenv-lxqt-sddm.downloads`
The included packages are:

1. `sddm` (since `v0.1.0`)
2. `lxqt` (since `v0.1.0`)
3. `lxqt-core` (since `v0.1.0`)
4. `lxqt-qtplugin` (since `v0.1.0`)
5. `lxqt-powermanagement` (since `v0.1.0`)



## `deskenv-lxqt-sddm.postinst`
The script is instructed for installing all the packages listed in `.downloads`
section. This only works after `pkgsel/include` stage where postinst would makes
a lot of sense.

![Granatum Banner](.sites/static/img/logo/banner-2500x1250-color.png)

# Granatum Project
Granatum project is a consolidated recipes for building custom Debian ISO. Its
aim is to unify multiple Debian OS image builds under a single workspace,
providing smooth Debian installations across wide variety of deployments.


## Documentations
This is Granatum code repository. For more information and documentations about
Granatum, please visit: https://zoralab.gitlab.io/granatum.

### Host Locally
To bring up the documentation server on your local system in offline mode,
while online,

1. please ensures [Hugo Extended](https://gohugo.io/getting-started/installing/)
is installed in your system.
2. execute the following:

```bash
$ cd .sites
$ ./manager.sh -s
$ ./manager.sh -r # test run the local server
```

Visit the `localhost` URL displayed by the `manager.sh` using your web
browser. If everything is working fine, you're ready for offline-mode.

Keep in mind that certain features (e.g. `emoji`) and pages
(e.g. `projects' status`) will not work as they requires online synchonizations.

#### Running Offline
To bring up the server again while offline, simply run:

```bash
$ cd .sites
$ ./manager.sh -r
```

and visit the `localhost` URL through your web browser.

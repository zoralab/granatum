#!/bin/bash
################################
# User Variables               #
################################
VERSION="0.0.1"

################################
# App Variables                #
################################
action=""
changelogger="changelogger.sh"
scripts_directory=".scripts"
current_directory="$PWD"
update_component=""

version_path="VERSION"
changelog_version=""
changelog_path="CHANGELOG.md"
changelog_ref="staging"
changelog_tgt="master"
changelog_pty="low"


################################
# Functions                    #
################################
print_version() {
	echo $VERSION
}

_print_status() {
	tag="INFO"
	case "$1" in
	warning|WARNING|Warning)
		tag="WARNING"
		;;
	error|ERROR|Error)
		tag="ERROR"
		;;
	debug|DEBUG|Debug)
		tag= "DEBUG"
		;;
	*)
		;;
	esac

	echo -e "[ $tag ] ${@:2}"
	unset $tag
}

_initialization() {
	script_directory="${current_directory}/${scripts_directory}"
	if [ ! -d "$script_directory" ]; then
		_print_status error "$script_directory not present."
		exit 1
	fi

	changelogger="${script_directory}/${changelogger}"
	if [ ! -f "$changelogger" ]; then
		_print_status error "$changelogger not present."
		exit 1
	fi

	changelog_path="${current_directory}/${changelog_path}"

	version_path="${current_directory}/${version_path}"
	if [ ! -f "$version_path" ]; then
		_print_status error "$version_path not present."
		exit 1
	fi
}

_update_changelog() {
	changelog_version="$(cat "$version_path")"

	$changelogger \
		--update "$changelog_version" \
		--reference "$changelog_ref" \
		--target "$changelog_tgt" \
		--priority "$changelog_pty" \
		--markdown "$changelog_path"
}

update() {
	_initialization

	case "$update_component" in
	changelog)
		_update_changelog
		;;
	*)
		_print_status error "no component specified for update."
		exit 1
		;;
	esac
}

################################
# CLI Parameters and Help      #
################################
print_help() {
	echo "\
PROGRAM NAME
One liner description
-------------------------------------------------------------------------------
To use: $0 [ACTION] [ARGUMENTS]

ACTIONS
1. -h, --help			print help. Longest help is up
				to this length for terminal
				friendly printout.

2. -u, --update			generate repository components.
				COMPULSORY VALUES (choose one)
				1. --update changelog
					update current changelog.

3. -v, --version		print app version.
"
}

run_action() {
case "$action" in
"h")
	print_help
	;;
"u")
	update
	;;
"v")
	print_version
	;;
*)
	echo "[ ERROR ] - invalid command."
	return 1
	;;
esac
}

process_parameters() {
while [[ $# != 0 ]]; do
case "$1" in
-u|--update)
	action="u"
	if [[ "$2" != "" && "${2:0:1}" != "-" ]]; then
		update_component="$2"
		shift 1
	fi
	;;
-h|--help)
	action="h"
	;;
-v|--version)
	action="v"
	;;
*)
	;;
esac
shift 1
done
}

main() {
	process_parameters $@
	run_action
	if [[ $? != 0 ]]; then
		exit 1
	fi
}

if [[ $BASHELL_TEST_ENVIRONMENT != true ]]; then
	main $@
fi

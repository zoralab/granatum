#!/bin/bash
################################
# User Variables               #
################################
VERSION="0.1.0"
mod_name="${GRANATUM_MODULE:-""}"

################################
# App Variables                #
################################
action=""
current_directory="$PWD"
local_pkg_path="${current_directory}/local_packages"
profiles_path="${current_directory}/profiles"
confs_path="${current_directory}/confs"
conf_filepath="${current_directory}/.config"
mod_root="${current_directory}/.modules"

################################
# Libraries                    #
################################
_print_status() {
	_status="INFO"

	case "$1" in
	warning|warn|Warning|Warn|WARNING|WARN)
		_status="WARNING"
		;;
	debug|Debug|DEBUG)
		_status="DEBUG"
		;;
	error|err|Error|Err|ERROR|ERR)
		_status="ERROR"
		;;
	*)
		: # do nothing. Leave it as INFO.
		;;
	esac
	shift 1
	1>&2 echo "[ $_status ] $@"

	unset _status
}


################################
# Functions                    #
################################
print_version() {
	echo $VERSION
}

clean() {
	rm "$conf_filepath" &> /dev/null
	rm "${profiles_path}/custom."* &> /dev/null
}

build() {
	# validate location
	if [ ! -d "$profiles_path" ]; then
		_print_status error \
		"missing profiles_path. Are you in workspace root directory?"
		exit 1
	fi

	# clean up
	clean

	# check module parameter
	if [ "$mod_name" != "" ]; then
		_print_status info "detected $mod_name module..."
		_target_path="${mod_root}/${mod_name}"
	fi

	# process module if available
	if [ -d "$_target_path" ]; then
		_print_status info "processing $mod_name module..."

		# use the .config from module
		_target="${_target_path}/.config"
		if [ -f "$_target" ]; then
			cp -u "$_target" "$conf_filepath" 2> /dev/null
		fi

		# copy all local packages to workspace
		_target="${_target_path}/${local_pkg_path##*/}"
		if [ -d "$_target" ]; then
			cp -R -u "$_target" "$local_pkg_path" 2> /dev/null
			rm "${local_pkg_path}/.gitkeep"
			touch "${local_pkg_path}/.gitkeep"
		fi

		# copy all custom profiles to workspace
		_target="${_target_path}/${profiles_path##*/}"
		if [ -d "$_target" ]; then
			cp "${_target}/custom."* "${profiles_path}/." \
				2> /dev/null
		fi
	elif [ ! -z "$_target_path" ]; then
		# specified module but is missing
		_print_status error \
			"wanted $mod_name module but missing $_target_path."
	fi
	unset _target _target_path


	# run the build sequences
	if [ ! -f "$conf_filepath" ]; then
		_print_status error "missing .conf file"
		exit 1
	fi
	_print_status info "found .config file"
	_print_status info "starting build engine..."
	build-simple-cdd --conf "$conf_filepath"
}

_module_init() {
	if [ ! -d "$mod_root" ]; then
		mkdir "$mod_root"
		touch "${mod_root}/.gitkeep"
	fi

	if [ "$mod_name" == "" ]; then
		_print_status error "missing module name."
		exit 1
	fi
}

_module_create() {
	_target_path="${mod_root}/${mod_name}"
	if [ -d "$_target_path" ]; then
		_print_status error "module $mod_name already there!"
		exit 1
	fi

	_print_status info "creating $mod_name module..."
	# create local_packages directory
	_target="${_target_path}/${local_pkg_path##*/}"
	mkdir -p "$_target"
	touch "${_target}/.gitkeep"

	# create profiles directory
	_target="${_target_path}/${profiles_path##*/}"
	mkdir -p "$_target"
	touch "${_target}/.gitkeep"

	# create conf file
	_target="${_target_path}/.config"
	cp "${confs_path}/generic-amd64.conf" "$_target"

	unset _target _target_path
}

_module_delete() {
	_target_path="${mod_root}/${mod_name}"
	if [ ! -d "$_target_path" ]; then
		_print_status error "no such module: $mod_name"
		unset _target_path
		exit 0
	fi

	read -p "CONFIRM DELETING MODULE $mod_name (YES/n)? " choice
	case "$choice" in
	YES)
		rm -rf "$_target_path"
		_print_status info "deleted $_target_path"
		;;
	*)
		_print_status info "aborted deletion."
		:
		;;
	esac
	unset _target_path
}

create_module() {
	_module_init
	_module_create
}

delete_module() {
	_module_init
	_module_delete
}

################################
# CLI Parameters and Help      #
################################
print_help() {
	echo "\
GRANATUM OPERATOR
To semi-automate the Simple-CDD Build Process
-------------------------------------------------------------------------------
To use: $0 [ACTION] [ARGUMENTS]

ACTIONS
1. -h, --help			print help. Longest help is up
				to this length for terminal
				friendly printout.

2. -b, --build			build the ISO image process with .config file
				in current directory.
				OPTIONAL VALUE
				1. $ export GRANATUM_MODULE=\"name\"
					Instruct build engine to use the
					specified module.
					You need to export the module name as
					exported environment variable before
					calling --build.

3. -cls, --clean                to clean the workspace.

4. -cm, --create-module		to create a module.
				COMPULSORY VALUE
				1. $ export GRANATUM_MODULE=\"name\"
					You need to export the module name as
					exported environment variable before
					calling --create-module.

5. -dm, --delete-module		to delete a module.
				COMPULSORY VALUE
				1. $ export GRANATUM_MODULE=\"name\"
					You need to export the module name as
					exported environment variable before
					calling --delete-module.

6. -v, --version		print app version.
"
}

run_action() {
case "$action" in
"b")
	build
	;;
"cls")
	clean
	;;
"cm")
	create_module
	;;
"dm")
	delete_module
	;;
"h")
	print_help
	;;
"v")
	print_version
	;;
*)
	echo "[ ERROR ] - invalid command."
	return 1
	;;
esac
}

process_parameters() {
while [[ $# != 0 ]]; do
case "$1" in
-b|--build)
	action="b"
	;;
-cls|--clean)
	action="cls"
	;;
-cm|--create-module)
	action="cm"
	if [ "$2" != "" ] && [ "${2:0:1}" != "-" ]; then
		mod_name="$2"
		shift 1
	fi
	;;
-dm|--delete-module)
	action="dm"
	if [ "$2" != "" ] && [ "${2:0:1}" != "-" ]; then
		mod_name="$2"
		shift 1
	fi
	;;
-h|--help)
	action="h"
	;;
-v|--version)
	action="v"
	;;
*)
	;;
esac
shift 1
done
}

main() {
	process_parameters $@
	run_action
	if [[ $? != 0 ]]; then
		exit 1
	fi
}

if [[ $BASHELL_TEST_ENVIRONMENT != true ]]; then
	main $@
fi

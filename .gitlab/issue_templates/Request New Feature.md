# Description
Describe WHAT is the new feature here.



## The Story Behind
Tell us about the WHY and your inspiration.



## Current Behavior
Explain the current behavior.



## Expected Behavior
Explain the expected behavior.



## List out the MUST HAVE Personality
1. Describe the things that are MUST HAVE.
2. List them out like the way it is here.
3. Remember to leave a space after the numbering.



## Severity
Level of Security: ***Significant***

<!--
OPTIONS

***Critical***
danger, hang, freeze, or kill computer

***Severe***
blocking and can't use. Can't workaround it.

***Significant***
quite a problem but still usable. Can workaround it.

***Not significant***
just some minor touch-up. Everything is fine.
-->



# Urgency
Level of Urgency: ***Code BLUE***

<!--
OPTIONS

***Code BLACK***
Somebody's life is at stake (e.g. medical equipment, national security)

***Code RED***
Immediate Attention. (e.g. My business is not running at all)

***Code BLUE***
Please plan out. (e.g. I can survive for now)

***Code GREEN***
Not urgent. (e.g. take your time)
-->


[comment]: # (Automation - Don't worry! Let's Cory takes over here.)
/label ~"Discussion" ~"New Feature"

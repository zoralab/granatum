# Version 0.3.0
## Thu, 20 Feb 2020 12:16:55 +0800 - (Urgency: low)
1. ea5395a docs/en-us/_index.md: rejected samba profile
2. f411cf4 docs/en-us*: churned all documentations for latest updates
3. c782bc9 profiles: added resolvconf profile
4. 615b100 profiles: added qbittorrent.packages
5. d62da6f profiles/ssh-open-server: added ssh-openssh-server.postinst
6. 7be79f8 .sites: port to bissetii 1.11.0
7. b039aeb .sites/manager.sh: updated manager to the latest
8. e3494ab profiles/*.postinst: fix _log dump issue
9. cce9302 profiles: added x-touchpad-tapping profile
10. 00de0ae profiles: added sddm-autologin profile
11. 2fe9d77 profiles: added curl profile
12. 6b14fc4 profiles: added wget profile
13. 7c0d0dc docs/.../specs/reading-materials.md: added d-i main salsa group
14. b54d52f docs/.../specs/reading-materials.md: added preseed link material

# Version 0.2.0
## Fri, 17 Jan 2020 12:24:52 +0800 - (Urgency: low)
1. 2a04c3a confs/generic-amd64.conf: added 5 seconds timeout for auto-install
2. b2fe2a9 profiles: added ssh-openssh-server profile
3. 1c4f1db profiles: added ssh-openssh-client profile
4. 1650305 profiles: added fw-ati profile
5. c17c511 docs/en-us/_index.md: fixed numbering typo
6. aad8e5b profiles: added fw-core profile
7. 9bf5a16 docs/.../specs/reading-materials.md: added more reading points
8. f272b67 profiles: added vim profile
9. 30987ab profiles/dpart-stdfde.preseed: added gpt and expert recipe
10. af02c76 .scripts/granatum.sh: added clean function
11. 253a6f7 docs/en-us/specifications: added creating-postinst.md
12. cb4448e profiles/*.postinst: use automated name
13. 26f7096 profiles: added gnupg profile
14. 3171991 docs/.../specs/reading-materials.md: added Debian installer spec
15. b24a945 docs/.../profiles/deskenv-lxqt-sddm.md: added status notice
16. fd9e811 docs/.../profiles/deskenv-lxqt-sddm.md: removed temporary notice
17. 0cf24e2 docs/en-us/_index.md: marked plymouth profile as completed
18. a846ed0 .scripts/granatum.sh: fixed false positive error message and printout
19. b00baed profiles: added plymouth profile
20. eea6990 profiles: added sshfs
21. bd587e3 docs/en-us/specifications: added reading-materials.md

# Version 0.1.0
## Wed, 08 Jan 2020 14:13:28 +0800 - (Urgency: low)
1. 3d6dc94 docs/en-us/_index.md: added automated post-install password reset
2. 5177e77 docs/en-us/profiles: re-label parent group to B category
3. db39497 root: added .modules feature for user's customizations
4. d94eabf .scripts/granatum.sh: refactored for argument and consistency
5. 8505d12 profiles: added backports-core profile
6. 0f55e53 profiles/ added deskenv-lxqt-sddm profile
7. 5efad13 profiles/template-custom.postinst: added _log function
8. df85807 confs/generic-amd64.conf: corrected typo
9. f3b8525 profiles: added template-custom.postinst
10. 53a62b9 README.md: updated README.md for local read-up
11. 85e2445 docs/en-us/projects/license.md: set to publish mode
12. f9f83d4 docs/en-us/_index.md: adds simple-cdd reference to introduction
13. f2a45c5 .sites/static: added favicon icons
14. 2c2a720 .sites/static: added banner and logo into static assets
15. 2f9183c profiles: added haveged profile
16. 8fb88b9 docs/en-us/profiles: added template-custom documentations
17. c6bdf8c docs/en-us/profiles: added ufw profile documentation
18. 98e7eac docs/en-us/profiles: added tree profile documentation
19. c5cbed4 docs/en-us/profiles/core.md: re-titled to match template
20. 37f1fb5 docs/en-us/profiles: grouped all bootloaders under bloader-X tag
21. 9036c27 docs/en-us/profiles: added dpart-stdfde profile documentation
22. 6da6f28 docs/en-us/profiles: updated all to match latest template
23. 1b1102e .sites/archetypes/profiles.md: facilitate dash names
24. 6dfbc23 .sites/archetypes/profiles.md: added template text for simple-cdd
25. cf3dedd docs/en-us/profiles: added nobootloader.md profile documentation
26. c981ad6 .sites/archetypes/profiles.md: added inclusion code blocks
27. bf567f7 docs/en-us/profiles: added lilo.md profile documentation
28. c688325 */profiles.md: added simple-cdd .conf inclusion documentation
29. f88a254 docs/en-us/profiles: added grub.md for documentation
30. 96040c0 .sites/archetypes: added profiles.md for profile documentation
31. 733226c docs/.../profiles: added core.md for documenting core profile
32. f62b100 root: added CONTRIBUTING.md guides
33. d534e2c docs/en-us/_index.md: added Debian Stable Suite feature into list
34. f5ae687 .scripts/ci.sh: corrected changelog_path filename
35. ee53418 root: added changelog automation tool
36. ed624a2 root: added .gitlab templates
37. 1dd98f1 .scripts/run.sh: renamed to granatum.sh for documentation
38. 9e5580c root: added README.md
39. 91f3e34 root: ported generic amd64 headless configurations and profiles
40. ffb23d2 profiles/core.udebs: corrected typo
41. 443162a confs/debian-amd64.conf: ported master settings into main conf
42. 6339b88 root: added basic core profiles
43. 6196280 root: initialize simple-cdd workspace
44. da4a585 .sites/config/_default/config.toml: updated base URL
45. 678fc65 .gitignore: added exlcusion to .d directory
